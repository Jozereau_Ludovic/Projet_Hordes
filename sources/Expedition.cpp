//
// Created by jozereau on 23/02/17.
//

#include "../ressources/Expedition.hpp"
#include "../ressources/outremonde/ville/Ville.hpp"
#include "../ressources/outremonde/OutreMonde.hpp"

int Expedition::compteur = 1;

Expedition::Expedition(int pa, int membresMax, Citoyen *citoyen) :
        id(compteur), paTotaux(pa), membresMax(membresMax), membres(std::vector<Citoyen *>()), trajet(std::vector<Case *>()) {

    if (pa > 1) {
        compteur++;

        if (pa%2 != 0) {
            pa--;
            paTotaux--;
        }
        membres.push_back(citoyen);

        OutreMonde *outreMonde = OutreMonde::getInstance();

        std::vector<Case *> cases = eloignementVille(outreMonde->getCase(citoyen->getPosX(), citoyen->getPosY()));
        std::vector<Case *> nouvCases;
        Case *pos = cases[rand() % cases.size()];

        while (pa-- > pos->getDistPa()) {
            trajet.push_back(pos);

            cases = eloignementVille(pos); // On cherche les cases pour s'éloigner de la ville
            nouvCases = filtreNouvellesCases(cases); // On filtre uniquement les nouvelles cases

            if (nouvCases.empty()) { // S'il n'y a pas de nouvelles cases
                cases = rapprochementVille(pos); // On cherche les cases pour se rapprocher
                nouvCases = filtreNouvellesCases(cases); // Et on les filtre

                if (nouvCases.empty()) {
                    pos = cases[rand() % cases.size()]; // On se rapproche
                }
                else {
                    pos = nouvCases[rand() % nouvCases.size()];
                }
            }
            else {
                pos = nouvCases[rand() % nouvCases.size()];
            }
        }

        pos = trajet.back();

        while (!(cases = rapprochementVille(pos)).empty()) {
            if ((nouvCases = filtreNouvellesCases(cases)).empty()) {
                pos = cases[rand() % cases.size()];
            }
            else {
                pos = nouvCases[rand() % nouvCases.size()];
            }
            trajet.push_back(pos);
        }
    }
    else {
        delete this;
    }
}

Expedition::~Expedition() {
    membres.clear();
    trajet.clear();
}

Case* Expedition::getCaseSuivante(int pos) {
    return trajet[pos];
}

bool Expedition::tousRentres() {
    for (Citoyen *citoyen : membres) {
        if (!citoyen->isEnVille()) {
            return false;
        }
    }
    return true;
}

void Expedition::ajouterMembre(Citoyen *citoyen) {
    OutreMonde::sortie << citoyen->getNom() << " rejoint l'expédition n°" << id << std::endl;
    membres.push_back(citoyen);
}

std::vector<Case *> Expedition::eloignementVille(Case *position) {
    std::vector<Case *> cases;

    OutreMonde *outreMonde = OutreMonde::getInstance();
    Case *haut = outreMonde->getCase(position->getX() - 1, position->getY());
    Case *bas = outreMonde->getCase(position->getX() + 1, position->getY());
    Case *gauche = outreMonde->getCase(position->getX(), position->getY() - 1);
    Case *droite = outreMonde->getCase(position->getX(), position->getY() + 1);


    if (haut != nullptr && haut->getDistPa() > position->getDistPa()) {
        cases.push_back(haut);
    }

    if (bas != nullptr && bas->getDistPa() > position->getDistPa()) {
        cases.push_back(bas);
    }

    if (gauche != nullptr && gauche->getDistPa() > position->getDistPa()) {
        cases.push_back(gauche);
    }

    if (droite != nullptr && droite->getDistPa() > position->getDistPa()) {
        cases.push_back(droite);
    }

    return cases;
}

std::vector<Case *> Expedition::rapprochementVille(Case *position) {
    std::vector<Case *> cases;

    OutreMonde *outreMonde = OutreMonde::getInstance();
    Case *haut = outreMonde->getCase(position->getX() - 1, position->getY());
    Case *bas = outreMonde->getCase(position->getX() + 1, position->getY());
    Case *gauche = outreMonde->getCase(position->getX(), position->getY() - 1);
    Case *droite = outreMonde->getCase(position->getX(), position->getY() + 1);


    if (haut != nullptr && haut->getDistPa() < position->getDistPa()) {
        cases.push_back(haut);
    }

    if (bas != nullptr && bas->getDistPa() < position->getDistPa()) {
        cases.push_back(bas);
    }

    if (gauche != nullptr && gauche->getDistPa() < position->getDistPa()) {
        cases.push_back(gauche);
    }

    if (droite != nullptr && droite->getDistPa() < position->getDistPa()) {
        cases.push_back(droite);
    }

    return cases;
}

std::vector<Case *> Expedition::filtreNouvellesCases(std::vector<Case *> cases) {
    std::vector<Case *> nouvCases;
    bool dejaParcourue;

    for (Case *pos : cases) {
        dejaParcourue = false;
        for (Case *parcourue : trajet) {
            if (pos == parcourue) {
                dejaParcourue = true;
                break;
            }
        }

        if (!dejaParcourue) {
            nouvCases.push_back(pos);
        }
    }

    return nouvCases;
}

bool Expedition::complete() {
    return membresMax == membres.size();
}