//
// Created by jozereau on 06/02/17.
//

#include <algorithm>

#include "../../ressources/objets/ListeObjets.hpp"

//ListeObjets *Singleton<ListeObjets>::instance = nullptr;

// Todo: revoir les raretés
ListeObjets::ListeObjets() {
    // Ajout des objets
    objets.push_back(new Objet(ObjetId::souche, "Souche de bois pourri", Rarete::debris));
    objets.push_back(new Objet(ObjetId::planche, "Planche tordue", Rarete::commun));
    objets.push_back(new Objet(ObjetId::poutre, "Poutre rafistolée", Rarete::rare));
    objets.push_back(new Objet(ObjetId::debris, "Débris métallique", Rarete::debris));
    objets.push_back(new Objet(ObjetId::ferraille, "Ferraille", Rarete::commun));
    objets.push_back(new Objet(ObjetId::structure, "Structure métallique", Rarete::rare));
    objets.push_back(new Objet(ObjetId::tube, "Tube de cuivre", Rarete::rare));
    objets.push_back(new Objet(ObjetId::scie, "Scie à métaux", Rarete::rare));
    objets.push_back(new Objet(ObjetId::lampe_off, "Lampe de chevet éteinte", Rarete::rare));
    objets.push_back(new Objet(ObjetId::pile, "Pile", Rarete::rare));
    objets.push_back(new Objet(ObjetId::radio_off, "Radio K7 éteint", Rarete::rare));
    objets.push_back(new Objet(ObjetId::produits, "Produits pharmaceutiques", Rarete::rare));
    objets.push_back(new Objet(ObjetId::allumettes, "Boîte d'allumettes", Rarete::rare));
    objets.push_back(new Objet(ObjetId::meuble_kit, "Meuble en kit", Rarete::rare));
    objets.push_back(new Objet(ObjetId::table, "Table Järpen", Rarete::rare));
    objets.push_back(new Objet(ObjetId::rocking, "Rocking Chair", Rarete::rare));
    objets.push_back(new Objet(ObjetId::porte, "Vieille porte", Rarete::rare));
    objets.push_back(new Objet(ObjetId::treteau, "Tréteau", Rarete::rare));
    objets.push_back(new Objet(ObjetId::mecanisme, "Mécanisme", Rarete::rare));
    objets.push_back(new Objet(ObjetId::pve, "Poignée de vis et écrous", Rarete::rare));
    objets.push_back(new Objet(ObjetId::rustine, "Rustine", Rarete::rare));
    objets.push_back(new Objet(ObjetId::explosifs, "Explosifs bruts", Rarete::rare));
    objets.push_back(new Objet(ObjetId::explosifs, "Explosifs bruts", Rarete::rare));
    objets.push_back(new Objet(ObjetId::detonateur, "Détonateur compact", Rarete::rare));
    objets.push_back(new Objet(ObjetId::composant, "Composant électronique", Rarete::rare));

    // Ajout des consommables
    consommables.push_back(new Consommable(ObjetId::ration, "Ration d'eau", Rarete::ration, 0, true));
    consommables.push_back(new Consommable(ObjetId::biscuit, "Biscuit fade", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::poulet, "Ailerons de poulet entamés", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::chewing, "Chewing-gums séchés", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::jambon_beurre, "Jambon-beurre moisi", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::napolitain, "Napolitains moisis", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::nouilles, "Nouilles chinoises", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::chips, "Paquet de chips molles", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::petit_beurre, "Petit-beurres rances", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::pims, "Pims périmés", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::legume, "Légume suspect", Rarete::introuvable, 0, false, true));
    consommables.push_back(new Consommable(ObjetId::steak, "Steak appétissant", Rarete::introuvable, 1, false, true));
    consommables.push_back(new Consommable(ObjetId::melon, "Melon d'intestin", Rarete::introuvable, 1, false, true));
    consommables.push_back(new Consommable(ObjetId::steak, "Cadavre du voyageur", Rarete::unique, 1, false, true));
    consommables.push_back(new Consommable(ObjetId::vodka, "Vodka marinostov", Rarete::rare, 0, false, false, true));
    consommables.push_back(new Consommable(ObjetId::steroides, "Stéroïdes anabolisants", Rarete::rare, 0, false, false, false, true));
    consommables.push_back(new Consommable(ObjetId::twinoide, "Twinoide", Rarete::epique, 2, false, false, false, true));
    consommables.push_back(new Consommable(ObjetId::mse, "Médicament sans étiquette", Rarete::rare,
                                           {{EffetId::rien, EffetId::rien}, {EffetId::pa, EffetId::rien},
                                            {EffetId::dependance, EffetId::pa_bonus}, {EffetId::terreur, EffetId::rien}}));
    consommables.push_back(new Consommable(ObjetId::os_charnu, "Os charnu", Rarete::introuvable,
                                           {{EffetId::pa, EffetId::rien}, {EffetId::pa, EffetId::infection}}));

    // Ajout des conteneurs
    conteneurs.push_back(new Conteneur(ObjetId::doggybag, "Doggy-bag", Rarete::rare,
                                       {{ObjetId::biscuit, 100/9}, {ObjetId::poulet, 100/9}, {ObjetId::chewing, 100/9},
                                        {ObjetId::jambon_beurre, 100/9}, {ObjetId::napolitain, 100/9}, {ObjetId::nouilles, 100/9},
                                        {ObjetId::chips, 100/9}, {ObjetId::petit_beurre, 100/9}, {ObjetId::pims, 100/9}},
                                       std::vector<ObjetId>()));
    conteneurs.push_back(new Conteneur(ObjetId::affaires, "Affaires d'un citoyen", Rarete::rare,
                                       {{ObjetId::lampe_off, 20}, {ObjetId::radio_off, 20},
                                        {ObjetId::pile, 20}, {ObjetId::produits, 20}, {ObjetId::allumettes, 20}},
                                       std::vector<ObjetId>()));

    // Armes
        // Sans recharge
    armes.push_back(new Arme(ObjetId::pampl_explo, "Pamplemousse explosif", Rarete::introuvable, 100, 100, 5, 9));
    armes.push_back(new Arme(ObjetId::canif, "Canif dérisoire", Rarete::rare, 75, 25, 1, 1));
    armes.push_back(new Arme(ObjetId::canif_casse, "Canif dérisoire", Rarete::introuvable, 75, 25, 1, 1, true));
    armes.push_back(new Arme(ObjetId::chaise, "Chaise Ektörp-Gluten", Rarete::rare, 60, 50, 1, 1));
    armes.push_back(new Arme(ObjetId::chaise_casse, "Chaise Ektörp-Gluten", Rarete::introuvable, 60, 50, 1, 1, true));
    armes.push_back(new Arme(ObjetId::clef_molette, "Clef à Molette", Rarete::rare, 20, 100/3, 1, 1));
    armes.push_back(new Arme(ObjetId::clef_molette_casse, "Clef à Molette", Rarete::introuvable, 20, 100/3, 1, 1, true));
    armes.push_back(new Arme(ObjetId::coupe_coupe, "Coupe-coupe", Rarete::rare, 25, 100, 2, 2));
    armes.push_back(new Arme(ObjetId::coupe_coupe_casse, "Coupe-coupe", Rarete::introuvable, 25, 100, 2, 2, true));
    armes.push_back(new Arme(ObjetId::couteau_dents, "Couteau à dents", Rarete::rare, 20, 100, 1, 1));
    armes.push_back(new Arme(ObjetId::couteau_dents_casse, "Couteau à dents", Rarete::introuvable, 20, 100, 1, 1, true));
    armes.push_back(new Arme(ObjetId::couteau_suisse, "Couteau suisse", Rarete::rare, 50, 100/3, 1, 1));
    armes.push_back(new Arme(ObjetId::couteau_suisse_casse, "Couteau suisse", Rarete::introuvable, 50, 100/3, 1, 1, true));
    armes.push_back(new Arme(ObjetId::cutter, "Cutter", Rarete::rare, 90, 50, 1, 1));
    armes.push_back(new Arme(ObjetId::cutter_casse, "Cutter", Rarete::introuvable, 90, 50, 1, 1, true));
    armes.push_back(new Arme(ObjetId::four, "Four cancérigène", Rarete::rare, 35, 100, 1, 1));
    armes.push_back(new Arme(ObjetId::four_casse, "Four cancérigène", Rarete::introuvable, 35, 100, 1, 1, true));
    armes.push_back(new Arme(ObjetId::baton, "Grand Bâton Sec", Rarete::rare, 70, 50, 1, 1));
    armes.push_back(new Arme(ObjetId::baton_casse, "Grand Bâton Sec", Rarete::introuvable, 70, 50, 1, 1, true));
    armes.push_back(new Arme(ObjetId::chaine, "Grosse chaîne rouillée", Rarete::rare, 30, 50, 1, 1));
    armes.push_back(new Arme(ObjetId::chaine_casse, "Grosse chaîne rouillée", Rarete::introuvable, 30, 50, 1, 1, true));
    armes.push_back(new Arme(ObjetId::tournevis, "Tournevis", Rarete::rare, 50, 25, 1, 1));
    armes.push_back(new Arme(ObjetId::tournevis_casse, "Tournevis", Rarete::introuvable, 50, 25, 1, 1, true));

        // A recharge
    armes.push_back(new Arme(ObjetId::lance_pile_1_charge, "Lance-Pile 1-PDTG", Rarete::introuvable, 0, 0, 1,
                             100, 1, 1, ObjetId::pile));
    armes.push_back(new Arme(ObjetId::lance_pile_1_vide, "Lance-Pile 1-PDTG", Rarete::rare, 0, 0, 1,
                             100, 1, 0, ObjetId::pile));
    armes.push_back(new Arme(ObjetId::pistolet_eau_3, "Pistolet à Eau", Rarete::introuvable, 0, 1, 1,
                             100, 3, 3, ObjetId::ration));
    armes.push_back(new Arme(ObjetId::pistolet_eau_2, "Pistolet à Eau", Rarete::introuvable, 0, 1, 1,
                             100, 3, 2, ObjetId::ration));
    armes.push_back(new Arme(ObjetId::pistolet_eau_1, "Pistolet à Eau", Rarete::introuvable, 0, 1, 1,
                             100, 3, 1, ObjetId::ration));
    armes.push_back(new Arme(ObjetId::pistolet_eau_vide, "Pistolet à Eau", Rarete::rare, 0, 1, 1,
                             100, 3, 0, ObjetId::ration));



    // Mise en commun des objets + tri
    for (Objet *objet : objets) {
        tousObjets.push_back(objet);
    }
    for (Objet *objet : consommables) {
        tousObjets.push_back(objet);
    }
    for (Objet *objet : conteneurs) {
        tousObjets.push_back(objet);
    }
    for (Objet *objet : armes) {
        tousObjets.push_back(objet);
    }
    std::stable_sort(tousObjets.begin(), tousObjets.end(), plusRare);
}

ListeObjets::~ListeObjets() {
    for (Objet *objet : tousObjets) {
        delete objet;
    }

    tousObjets.clear();
    objets.clear();
    consommables.clear();
    conteneurs.clear();
}

Objet *ListeObjets::getObjetsParId(ObjetId id) {
    Objet *objet = nullptr;
    Consommable *consommable = nullptr;
    Conteneur *conteneur = nullptr;
    Arme *arme = nullptr;

    if ((objet = this->getObjetParId<Objet *>(id, objets)) != nullptr) {
        return new Objet(*objet);
    }

    if ((consommable = this->getObjetParId<Consommable *>(id, consommables)) != nullptr) {
        return new Consommable(*consommable);
    }

    if ((conteneur = this->getObjetParId<Conteneur *>(id, conteneurs)) != nullptr) {
        return new Conteneur(*conteneur);
    }

    if ((arme = this->getObjetParId<Arme *>(id, armes)) != nullptr) {
        return new Arme(*arme);
    }

    return 0;
}

template <typename T>
T ListeObjets::getObjetParId(ObjetId id, std::vector<T> liste) {
    T objetVoulu = nullptr;

    for (T objet : liste) {
        if (objet->getId() == id) {
            objetVoulu = objet;
            break;
        }
    }

    return objetVoulu;
}

const std::vector<Objet*> ListeObjets::getObjetsParRarete(Rarete r) {
    std::vector<Objet*> res;
    bool tr = false;

    for (std::vector<Objet*>::iterator src = tousObjets.begin(); src != tousObjets.end(); src++) {
        if (tr && (*src)->getRarete() != r) break;

        if ((*src)->getRarete() == r) {
            res.push_back(getObjetsParId((*src)->getId()));
            tr = true;
        }
    }

    return res;
}