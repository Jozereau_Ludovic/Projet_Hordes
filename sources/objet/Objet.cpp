//
// Created by jozereau on 18/10/16.
//

#include "../../ressources/objets/Objet.hpp"

Objet::Objet() { }
Objet::Objet(ObjetId id, std::string nom, Rarete rarete, int destructibilite) :
        id(id), nom(nom), rarete(rarete), destructibilite(destructibilite) { }

Objet::Objet(const Objet &o) : id(o.id), nom(o.nom), rarete(o.rarete), destructibilite(o.destructibilite) { }

bool Objet::detruireObjet() {
    int de_100 = rand()%100 + 1;

    return de_100 <= destructibilite;

}
Objet::~Objet() {
    rarete = Rarete::introuvable;
}

TypeObjet Objet::utiliser(listeObjet inventaire) {
    return TypeObjet::objet;
}

bool plusRare(Objet *const &a, Objet *const &b) {
    return a->getRarete() > b->getRarete();
}