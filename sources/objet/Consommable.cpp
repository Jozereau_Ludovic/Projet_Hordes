//
// Created by jozereau on 08/12/16.
//
#include "../../ressources/objets/Consommable.hpp"

Consommable::Consommable(ObjetId id, std::string nom, Rarete rarete, listeEffets effets) :
        Objet(id, nom, rarete, 100), pa_bonus(0), pa_rendu(false), eau(false), nourriture(false), alcool(false), drogue(false),
        terreur(false), infection(false), soin(false), aleatoire(true), effets(effets), dependance(false) { }

Consommable::Consommable(ObjetId id, std::string nom, Rarete rarete, int pa, bool eau,
                         bool nourriture, bool alcool, bool drogue, bool terreur, bool infection, bool soin) :
        Objet(id, nom, rarete, 100), pa_bonus(pa), pa_rendu(true), eau(eau), nourriture(nourriture), alcool(alcool),
        drogue(drogue), terreur(terreur), infection(infection), soin(soin), aleatoire(false), effets(listeEffets()), dependance(false) { }

Consommable::Consommable(const Consommable &c) : Objet(c.id, c.nom, c.rarete, 100), pa_bonus(c.pa_bonus), pa_rendu(c.pa_rendu),
                                                 eau(c.eau), nourriture(c.nourriture), alcool(c.alcool), drogue(c.drogue),
                                                 terreur(c.terreur), infection(c.infection), soin(c.soin), aleatoire(c.aleatoire),
                                                 effets(c.effets), dependance(c.dependance) { }

Consommable::~Consommable() {
    rarete = Rarete::introuvable;
    effets.clear();
}

TypeObjet Consommable::utiliser(listeObjet inventaire) {
    if (aleatoire) {
        std::pair<EffetId, EffetId> effet = effets[rand()%effets.size()];

        resetEffets();
        effetsAleatoire(effet.first);
        effetsAleatoire(effet.second);
    }

    return TypeObjet::consommable;
}

void Consommable::resetEffets() {
    pa_bonus = 0;
    pa_rendu = false;
    eau = false;
    nourriture = false;
    alcool = false;
    drogue = true;
    terreur = false;
    soin = false;
}

void Consommable::effetsAleatoire(EffetId effet) {
    switch (effet) {
        case EffetId::rien :
            break;

        case EffetId ::dependance :
            dependance = true;
            break;

        case EffetId::pa :
            pa_bonus = 0;
            pa_rendu = true;
            break;

        case EffetId::pa_bonus :
            pa_bonus = 1;
            pa_rendu = true;
            break;

        case EffetId::terreur :
            terreur = true;
            break;

        case EffetId::infection :
            infection = true;
    }
}