//
// Created by jozereau on 20/02/17.
//

#include "../../ressources/objets/Conteneur.hpp"
#include "../../ressources/objets/ListeObjets.hpp"

Conteneur::Conteneur(ObjetId id, std::string nom, Rarete rarete, std::vector<std::pair<ObjetId, float>> drops,
                     std::vector<ObjetId> outils) :
        Objet(id, nom, rarete, 100), drops(drops), outils(outils), drop(nullptr) { }
Conteneur::Conteneur(const Conteneur &c) : Objet(c.id, c.nom, c.rarete, 100), drops(c.drops), outils(c.outils), drop(nullptr) { }
Conteneur::~Conteneur() {
    drops.clear();
    outils.clear();

    drop = nullptr;
}

TypeObjet Conteneur::utiliser(listeObjet inventaire) {
    if (outilPresent(inventaire)) {
        int de_100 = rand() % 100 + 1;
        float somme = 0.0f;

        ObjetId dropId = drops[0].first;

        for (std::pair<ObjetId, float> paires : drops) {
            somme += paires.second;
            if (somme < de_100) {
                dropId = paires.first;
            }
            else {
                drop = ListeObjets::getInstance()->getObjetsParId(dropId);
                //drop = ListeObjets::getInstance()->getObjetsParId(dropId);
            }
        }

        drop = ListeObjets::getInstance()->getObjetsParId(dropId);
    }
    return TypeObjet::conteneur;
}

bool Conteneur::outilPresent(listeObjet inventaire) {
    if (outils.empty()) {
        return true;
    }

    for (ObjetId id : outils) {
        for (Objet *objet : inventaire) {
            if (objet->getId() == id) {
                return true;
            }
        }
    }

    return false;
}