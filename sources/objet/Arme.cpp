//
// Created by jozereau on 05/03/17.
//

#include "../../ressources/objets/Arme.hpp"

Arme::Arme(ObjetId id, std::string nom, Rarete rarete, int destructibilite, float probaTouche, int minTue, int maxTue,
           bool casse) :
        Objet(id, nom, rarete, destructibilite), probaTouche(probaTouche), minTue(minTue), maxTue(maxTue),
        recharge(false), probaDecharge(0), chargesMax(0), charges(0), munitions(ObjetId::scie), casse(casse) { }
Arme::Arme(ObjetId id, std::string nom, Rarete rarete, int destructibilite, int minTue, int maxTue, float probaDecharge,
           int chargesMax, int charges, ObjetId munitions) :
        Objet(id, nom, rarete, destructibilite), probaTouche(100), minTue(minTue), maxTue(maxTue), recharge(true),
        probaDecharge(probaDecharge), chargesMax(chargesMax), charges(charges), munitions(munitions), casse(false) { }
Arme::Arme(const Arme &a) :
        Objet(a.id, a.nom, a.rarete, a.destructibilite), probaTouche(a.probaTouche), minTue(a.minTue), maxTue(a.maxTue),
        recharge(a.recharge), probaDecharge(a.probaDecharge), chargesMax(a.chargesMax), charges(a.charges),
        munitions(a.munitions), casse(a.casse) { }

// Méthodes privées
int Arme::attaquerNonRecharge() {
    if (!casse) {
        if (rand()%100 + 1 <= probaTouche) {
            if (detruireObjet()) {
                casse = true;
                id = static_cast<ObjetId>((int) id + 1);
            }

            return rand() % (maxTue - minTue + 1) + minTue;
        }
    }

    return 0;
}

int Arme::attaquerRecharge() {
    if (!isVide()) {
        if (rand()%100 + 1 <= probaDecharge) {
            charges--;
            id = static_cast<ObjetId>((int) id + 1);
        }

        return rand() % (maxTue - minTue + 1) + minTue;
    }

    return 0;
}
// Fin méthodes privées

int Arme::nombreTues() {
    if (!recharge) {
        return attaquerNonRecharge();
    }

    return attaquerRecharge();
}

bool Arme::isVide() {
    return charges == 0;
}

void Arme::recharger() {
    if (!casse && recharge) {
        charges = chargesMax;
        id = static_cast<ObjetId>((int) id - chargesMax);
    }
}

TypeObjet Arme::utiliser(listeObjet inventaire) {
    return TypeObjet::arme;
}