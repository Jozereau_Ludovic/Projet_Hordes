//
// Created by jozereau on 26/10/16.
//
#include "../../ressources/divers/outils.hpp"

void transfertObjet(listeObjet depart, listeObjet arrivee, Objet *objet) {
    listeObjet::iterator i;
    for (i = depart.begin(); i != depart.end() && *i != objet; ++i);

    if (*i == objet) {
        arrivee.push_back(*i);
        depart.erase(i);
    }
}