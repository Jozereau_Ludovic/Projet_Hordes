//
// Created by dyag on 2/6/17.
//

#include "../../ressources/divers/Gauss.h"
// 0.5,0.45 : 1.5,0.4 : 2.5,0.4 : 3.5,0.7

Gauss::Gauss() {
}

Gauss::~Gauss() { }

float Gauss::geneGauss(float x, float y) {
    std::normal_distribution<float> distribution(x,y);

    return distribution(gene);
}