//
// Created by jozereau on 09/02/17.
//

#include "../../ressources/comportement/Comportement.hpp"
#include "../../ressources/outremonde/ville/Citoyen.hpp"
#include "../../ressources/outremonde/ville/chantiers/Chantiers.hpp"
#include "../../ressources/outremonde/ville/Portes.hpp"
#include "../../ressources/outremonde/OutreMonde.hpp"

Comportement::Comportement(Citoyen *citoyen) : citoyen(citoyen), prochaineAction(ActionId::debut), expedition(nullptr),
        posTrajet(0), fouilleAuto(0), fouille(false) { }
Comportement::~Comportement() {
    citoyen = nullptr;
    expedition = nullptr;
}

void Comportement::execute() {
    if (fouilleAuto > 0) {
        fouilleAuto--;
    }

    switch (prochaineAction) {
        case ActionId::debut :
            break;

        case ActionId::entrer :
            this->entrer();
            break;

        case ActionId::sortir :
            this->sortir();
            break;

        case ActionId::rechargerPa :
            this->rechargerPa();
            break;

        case ActionId::fouiller :
            this->fouiller();
            break;

        case ActionId::seDeplacer :
            this->seDeplacer();
            break;

        case ActionId::expedition :
            this->creerExpedition();
            break;

        case ActionId::ranger :
            this->ranger();
            break;

        case ActionId::preparation :
            this->preparation();
            break;

        case ActionId::ressources :
            this->ressources();
            break;

        case ActionId::construire :
            this->construire();
            break;

        case ActionId::atelier :
            this->atelier();
            break;

        case ActionId::ramasser :
            this->ramasser();
            break;

        case ActionId::tourGuet :
            this->tourGuet();
            break;

        case ActionId::fin :
            break;
    }
}


void Comportement::entrer() {
    if (!citoyen->isEnVille() && citoyen->getPosX() == 0 && citoyen->getPosY() == 0) {
        citoyen->entrer();

        if (expedition != nullptr && expedition->tousRentres()) {
            Ville::getVille()->supprimerExpedition(expedition->getId());
            expedition = nullptr;
        }

        this->prochaineAction = ActionId::ranger;
        posTrajet = 0;
    }
}

void Comportement::sortir() {
    if (citoyen->isEnVille()) {
        if (!Portes::getInstance()->isOuvertes()) {
            citoyen->ouvrirPortes();
        }
        citoyen->sortir();
        this->prochaineAction = ActionId::seDeplacer;
    }
}

void Comportement::seDeplacer() {
    Case *prochaineCase = nullptr;
    fouilleAuto = 0;
    fouille = false;

    if (citoyen->getPa() == 0) {
        this->prochaineAction = ActionId::rechargerPa;
    }
    else {
        if (OutreMonde::getInstance()->getCase(citoyen->getPosX(), citoyen->getPosY())->getZombies() > 0 &&
            citoyen->getArme()) {
            citoyen->attaquer();
        }
        if (expedition != nullptr && posTrajet != this->expedition->getPaTotaux() &&
                 (prochaineCase = this->expedition->getCaseSuivante(posTrajet++)) != nullptr) {
            citoyen->seDeplacer(prochaineCase);
            this->prochaineAction = ActionId::fouiller;
        }
        else {
            this->prochaineAction = ActionId::entrer;
        }
    }
}

void Comportement::ranger() {
    for (Objet *objet : citoyen->getInventaire()) {
        if (objet->getTypeObjet() == TypeObjet::conteneur) {
            citoyen->utiliserObjet(objet->getId());
        }
    }
    while (!citoyen->getInventaire().empty()) {
        citoyen->poserBanque(citoyen->getInventaire()[0]);
    }

    this->prochaineAction = ActionId::construire;
}

void Comportement::ramasser() {
    if (!OutreMonde::getInstance()->getCase(citoyen->getPosX(), citoyen->getPosY())->isVide() &&
        citoyen->restePlaceInventaire()) {
        Objet *obj = OutreMonde::getInstance()->getCase(citoyen->getPosX(), citoyen->getPosY())->getObjetAuSol()[0];
        citoyen->ramasserObjetDehors(obj);
        this->prochaineAction = ActionId::ramasser;
    }
   /* else if (OutreMonde::getInstance()->getCase(citoyen->getPosX(), citoyen->getPosY())->rareteSuperieure(citoyen->getInventaire())) {
        citoyen->prendreObjetRare();
    }*/
    else {
        this->prochaineAction = ActionId::seDeplacer;
    }
}

void Comportement::tourGuet() {
    TourGuet *tourGuet = Chantiers::getInstance()->getTourGuet();

    if (tourGuet->isConstruit() && tourGuet->getPrecision() != 1) {
        tourGuet->scruter();
        OutreMonde::sortie << citoyen->getNom() << " scrute les alentours. Nouvelle prévision maximum de " <<
                                          tourGuet->getPrevisionMax() << " zombies." << std::endl;
    }
}

Construction* Comportement::choisirConstruction() {
    Construction *cons = Chantiers::getInstance()->getRecommande();

    if (cons != nullptr) { // Si un chantier est recommandé
        if (cons->estConstructible()) { // S'il est constructible
            return cons;
        }

        while (!cons->getPrerequisNonConstruits().empty()) { // S'il a des prérequis à constuire
            if (!cons->getPrerequisConstructibles().empty()) { // S'il y en a constructible
                return cons->getPrerequisConstructibles()[0];
            }
            else { // Si aucun n'est constructible
                cons = Chantiers::getInstance()->getConstructionParId(cons->getPrerequis()[0]); // On tente de construire le premier prerequis
            }
        }

        // Si on trouve une construction dont les prérequis sont remplis mais non constructible, il manque des ressources
        if (Chantiers::getInstance()->getAtelier()->isConstruit()) { // Si l'atelier est construit
            while (!Chantiers::getInstance()->getAtelier()->ressourcesUtiles(cons).empty() && // Tant qu'il y a des ressources utiles pour la constrcution
                   citoyen->transformerRessources(Chantiers::getInstance()->getAtelier()->ressourcesUtiles(cons)[0])) { // Et que l'on a assez de pa pour transformer, on transforme
                if (cons->estConstructible() || citoyen->getPa() == 0) { // Si la construction devient constructible ou qu'on a plus de pa
                    return cons;
                }
            }
        }
    }

    if (!Chantiers::getInstance()->getConstructibles().empty()) { // Si d'autres constructions sont disponibles
        return Chantiers::getInstance()->getConstructibles()[0];
    }
    else { // S'il n'y a rien du tout
        return nullptr;
    }
}