//
// Created by jozereau on 23/02/17.
//

#include "../../ressources/comportement/CompExplorateur.hpp"
#include "../../ressources/outremonde/ville/Ville.hpp"
#include "../../ressources/outremonde/OutreMonde.hpp"

CompExplorateur::CompExplorateur(Citoyen *citoyen) : Comportement(citoyen) {
    this->prochaineAction = ActionId::tourGuet;
}

void CompExplorateur::rechargerPa() {
    if (!citoyen->isRassasie() && citoyen->getManger()) {
        citoyen->manger();
        this->prochaineAction = ActionId::seDeplacer;
    }
    else if (!citoyen->isDesaltere() && citoyen->getBoire()){
        citoyen->boire();
        this->prochaineAction = ActionId::seDeplacer;
    }
    else if (citoyen->getPosX() == 0 && citoyen->getPosY() == 0) {
        this->prochaineAction = ActionId::entrer;
    }
    else {
        this->prochaineAction = ActionId::fin;
    }
}

void CompExplorateur::fouiller() {
    if ((citoyen->getPosX() == 0 && citoyen->getPosY() == 0) ||
        OutreMonde::getInstance()->getCase(citoyen->getPosX(), citoyen->getPosY())->isEpuisee() ||
        OutreMonde::getInstance()->isNuit()) {

        if (!fouille) {
            this->prochaineAction = ActionId::fouiller;
            fouille = true;
        }
        else {
            this->prochaineAction = ActionId::ramasser;
        }
    }
    else {
        if (fouilleAuto == 0) {
            fouilleAuto = 120;
            citoyen->fouiller();
            this->prochaineAction = ActionId::fouiller;
        }
    }
}

void CompExplorateur::preparation() {
    if (!OutreMonde::getInstance()->isNuit()) {
        if (!Puits::getInstance()->isVide()) {
            citoyen->prendreRation();
        }
        else if (citoyen->getEau() != Eau::aucun) {
            citoyen->prendreEauBanque();
        }
        citoyen->prendreNourriture();
        citoyen->prendreArme();
        this->prochaineAction = ActionId::expedition;
    }
}

void CompExplorateur::creerExpedition() {
    expedition = nullptr;
    int paDispo = 6;

    if (citoyen->getManger()) {
        paDispo += 6;
    }
    if (citoyen->getBoire()) {
        paDispo += 6;
    }

    if (!Ville::getVille()->getExpeditions().empty()) {
        for (Expedition *expe : Ville::getVille()->getExpeditions()) {
            if (!expe->complete() && expe->getPaTotaux() <= paDispo) {
                this->expedition = expe;
                expe->ajouterMembre(citoyen);
                break;
            }
        }
    }
    if (expedition == nullptr) {
        Ville::getVille()->ajouterExpedition(new Expedition(paDispo, 6, citoyen));
        this->expedition = Ville::getVille()->getExpeditions().back();
    }

    this->prochaineAction = ActionId::sortir;
}

void CompExplorateur::tourGuet() {
    Comportement::tourGuet();
    this->prochaineAction = ActionId::preparation;
}

void CompExplorateur::reinitialiser() {
    this->prochaineAction = ActionId::tourGuet;
}

void CompExplorateur::ressources() {

}

void CompExplorateur::construire() {
    this->prochaineAction = ActionId::fin;
}

void CompExplorateur::atelier() {

}
