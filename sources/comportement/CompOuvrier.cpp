//
// Created by jozereau on 09/02/17.
//

#include "../../ressources/outremonde/ville/Citoyen.hpp"
#include "../../ressources/comportement/CompOuvrier.hpp"
#include "../../ressources/outremonde/ville/chantiers/Chantiers.hpp"
#include "../../ressources/outremonde/OutreMonde.hpp"
#include "../../ressources/outremonde/ville/chantiers/TourGuet.hpp"

CompOuvrier::CompOuvrier(Citoyen *citoyen) : Comportement(citoyen) {
    this->prochaineAction = ActionId::tourGuet;
}

void CompOuvrier::rechargerPa() {
    if (!citoyen->isDesaltere()) {
        citoyen->boire();
    }

    if (citoyen->isEnVille()) {
        this->prochaineAction = ActionId::construire;
    }
    else if (citoyen->getPosX() == 0 && citoyen->getPosY() == 0) {
        this->prochaineAction = ActionId::entrer;
    }
}

void CompOuvrier::fouiller() {
    if (!OutreMonde::getInstance()->isNuit()) {
        if (fouilleAuto == 0) {
            fouilleAuto = 120;
            citoyen->fouiller();
            this->prochaineAction = ActionId::fouiller;
        }
    }
    else {
        if (!fouille) {
            this->prochaineAction = ActionId::fouiller;
            fouille = true;
        }
        else {
            this->prochaineAction = ActionId::ramasser;
        }
    }
}

void CompOuvrier::ranger() {
    Comportement::ranger();

    this->prochaineAction = ActionId::preparation;
}

void CompOuvrier::preparation() {
    if (citoyen->getRation() != 1) {
        if (!Puits::getInstance()->isVide()) {
            citoyen->prendreRation();
        }
        else if (citoyen->getEau() != Eau::aucun) {
            citoyen->prendreEauBanque();
        }

        if (citoyen->getEau() == Eau::aucun) {
            citoyen->poserBanque(citoyen->getInventaire(ObjetId::ration));
        }
    }

    this->prochaineAction = ActionId::construire;
}

void CompOuvrier::ressources() {
    Ville::getVille()->ajouterExpedition(new Expedition(citoyen->getPa(), 1, citoyen));
    expedition = Ville::getVille()->getExpeditions().back();
    this->prochaineAction = ActionId::sortir;
}

void CompOuvrier::construire() {
    Construction *cons = this->choisirConstruction();

    if (cons != nullptr) { // S'il y a une construction
        citoyen->construireChantier(cons, citoyen->getPa());

        if (citoyen->getPa() != 0) {
            this->prochaineAction = ActionId::construire;
        }
        else if (citoyen->getInventaire(ObjetId::ration) != nullptr && !citoyen->isDesaltere()
                 && citoyen->getEau() != Eau::aucun) {
            this->prochaineAction = ActionId::rechargerPa;
        }
        else {
            this->prochaineAction = ActionId::fin;
        }
    }
    else {
        this->prochaineAction = ActionId::atelier;
    }
}

void CompOuvrier::atelier() {
    if (Chantiers::getInstance()->getAtelier()->isConstruit()) {
        auto ressources = Chantiers::getInstance()->getAtelier()->ressourcesTransformables();

        if (!ressources.empty()) {
            if (citoyen->transformerRessources(ressources[rand()%ressources.size()].second)) {
                this->prochaineAction = ActionId::atelier;
            }
            else if (citoyen->getPa() >= 2) {
                this->prochaineAction = ActionId::ressources;
            }
            else if (citoyen->getInventaire(ObjetId::ration) != nullptr && !citoyen->isDesaltere()
                     && citoyen->getEau() != Eau::aucun) {
                this->prochaineAction = ActionId::rechargerPa;
            }
            else {
                this->prochaineAction = ActionId::fin;
            }
        }
        else if (citoyen->getPa() >= 2) {
            this->prochaineAction = ActionId::ressources;
        }
        else {
            this->prochaineAction = ActionId::fin;
        }
    }
    else if (citoyen->getPa() >= 2) {
        this->prochaineAction = ActionId::ressources;
    }
    else {
        this->prochaineAction = ActionId::fin;
    }
}

void CompOuvrier::tourGuet() {
    Comportement::tourGuet();
    this->prochaineAction = ActionId::expedition;
}

void CompOuvrier::creerExpedition() {
    if (!OutreMonde::getInstance()->isNuit()) {
        Ville::getVille()->ajouterExpedition(new Expedition(2, 1, citoyen));
        expedition = Ville::getVille()->getExpeditions().back();
        this->prochaineAction = ActionId::sortir;
    }
}

void CompOuvrier::reinitialiser() {
    this->prochaineAction = ActionId::tourGuet;
}