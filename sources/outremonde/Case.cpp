//
// Created by dyag on 20/10/16.
//

#include <iostream>
#include <cmath>

#include "../../ressources/outremonde/Case.hpp"
#include "../../ressources/objets/Objet.hpp"
#include "../../ressources/divers/Gauss.h"
#include "../../ressources/outremonde/ville/Ville.hpp"
#include "../../ressources/objets/ListeObjets.hpp"

void Case::tuerZombie(int nb) {
    std::max(zombies -= nb, 0);

    this->isControlee();
}

Objet* Case::fouille() {
    double dist;

    dist = (!epuisee) ? (double) ((int)(sqrt(pow(x,2)+pow(y,2))) + 0.5) : 0;
    dist = std::min(dist,3.5);

    int rareteVal = (int)Gauss::getInstance()->geneGauss(dist,(dist>2.5) ? 0.7 : 0.4);
    if (dist == 0)
        rareteVal = (int)Gauss::getInstance()->geneGauss(0,0);

    if (nbObjet!=0) {
        nbObjet--;
    }
    if (nbObjet == 0) {
        epuisee = true;
    }

    Rarete rar = Rarete::commun;
    switch (rareteVal) {
        case 1:
            rar = Rarete::commun;
            break;
        case 2:
            rar = Rarete::rare;
            break;
        case 3:
            rar = Rarete::epique;
            break;
        case 4:
            rar = Rarete::unique;
            break;
        case 0:
            rar = Rarete::debris;
            break;
    }

    std::vector<Objet*> pool = ListeObjets::getInstance()->getObjetsParRarete(rar);

    Objet *drop = nullptr;

    if (!pool.empty()) {
        drop = pool[rand()%pool.size()];

        for (Objet *obj : pool) {
            if (obj != drop) {
                delete obj;
                obj = nullptr;
            }
        }
        pool.clear();
    }

    return drop;
}

listeObjet Case::getObjetAuSol() {
    return sol;
}

void Case::deposerObjet(Objet * obj) {
    sol.push_back(obj);

    std::stable_sort(sol.begin(), sol.end(), plusRare);
}

void Case::ramasserObjet(Objet *objet) {
   for (listeObjet::iterator i = sol.begin(); i != sol.end(); ++i) {
       if (*i == objet) {
           *i = nullptr;
           sol.erase(i);
           break;
       }
   }
}

bool Case::rareteSuperieure(listeObjet inventaire) {
    for (Objet *obj : sol) {
        for (Objet *inv : inventaire) {
            if (plusRare(obj, inv)) {
                return true;
            }
        }
    }

    return false;
}

void Case::changerPointControle(int ajout) {
    pointControle += ajout;
    this->isControlee();
}

bool Case::isControlee() {
    caseControlee = pointControle >= zombies || tempsControle > 0;

    if (pointControle >= zombies) {
        tempsControle = 30;
    }
    else {
        tempsControle--;
    }

    return tempsControle > 0;
}

// Constructeur et Destructeur
Case::Case(int x, int y):
x(x), y(y), epuisee(false), zombies(0), newZombie(0), nbObjet(10), batiment(false), ruine(nullptr),
distPa(std::abs(x) + std::abs(y)), caseControlee(true), tempsControle(0), pointControle(0) {

}

Case::~Case() {
    if (ruine) {
        delete ruine;
        ruine = nullptr;
    }

    for (Objet *objet : sol) {
        delete objet;
    }
    sol.clear();
}


// Getter et Setter
const bool Case::getBatiment() {
    return batiment;
}

void Case::setBatiment() {
    batiment = true;
    ruine = new Batiment;

}

const int Case::getZombies() {
    return zombies;
}

void Case::setZombies(int x) {
    zombies = x;
}

const int Case::getNewZombies() {
    return newZombie;
}

void Case::setNewZombies(int x) {
    newZombie = x;
}

bool Case::isVide() {
    return sol.empty();
}