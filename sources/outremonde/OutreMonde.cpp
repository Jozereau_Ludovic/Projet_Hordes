//
// Created by dyag on 20/10/16.
//


#include <fstream>
#include "../../ressources/outremonde/OutreMonde.hpp"
#include "../../ressources/objets/ListeObjets.hpp"
#include "../../ressources/divers/Gauss.h"
#include "../../ressources/comportement/CompExplorateur.hpp"
#include "../../ressources/comportement/CompOuvrier.hpp"
// Constructeur et Destructeur

std::ofstream OutreMonde::sortie = std::ofstream("./Ville.txt");;

OutreMonde::OutreMonde():
taille(generateurAleatoireInt(6, 9)), posVilleX(generateurAleatoireInt(2, taille - 2)),
posVilleY(generateurAleatoireInt(2, taille - 2)), temps(0), nuit(true), jour(1), multiplicateur(0) {
    desert.resize(taille);

    for (int i=0;i<taille;i++) {
        desert[i].resize(taille);

        for (int j=0;j<taille;j++) {
            desert[i][j] = new Case(i-posVilleX,j-posVilleY);
        }
    }

    // Placement des batiments
    for(int i=0;i<5;i++) {
        int x = generateurAleatoireInt(0, taille);
        int y = generateurAleatoireInt(0, taille);

        if (!(desert[x][y]->getBatiment())) {
            desert[x][y]->setBatiment();
        } else {
            i--;
        }
    }

    // Placement des zombies
    for (int i=0;i<taille;i++) {
        for (int j=0;j<taille;j++) {

            if (i!=posVilleX || j!=posVilleY) {
                // La case actuelle n'est pas la ville (le traitement pourra se faire différemment)
                desert[i][j]->setZombies(0);

                if (desert[i][j]->getBatiment()) {
                    // Batiment
                    desert[i][j]->setZombies(desert[i][j]->getZombies() + generateurAleatoireInt(7, 11));

                } else if ((i > 0 && desert[i - 1][j]->getBatiment()) ||
                           (i < taille-1 && desert[i + 1][j]->getBatiment()) ||
                           (j > 0 && desert[i][j - 1]->getBatiment()) ||
                           (j < taille-1 && desert[i][j + 1]->getBatiment())) {
                    // Adjacent à un batiment
                    desert[i][j]->setZombies(desert[i][j]->getZombies() + generateurAleatoireInt(4, 7));

                } else {
                    // La case est au milieu de nulle part
                    if (generateurAleatoireInt(0, 100) < 5) {
                        desert[i][j]->setZombies(desert[i][j]->getZombies() + generateurAleatoireInt(1, 2));
                    }
                }
            }

        }
    }

    std::vector<Citoyen *> citoyens;

    std::ifstream fichier("../../noms.txt", std::ios::in);

    for (int i = 0; i < 40; ++i) {
        std::string nom;
        fichier >> nom;
        citoyens.push_back(new Citoyen(i, nom));

        // Todo: revoir les attributions
        if (i%2 == 0) {
            citoyens.back()->setComportement(new CompOuvrier(citoyens.back()));
        }
        else {
            citoyens.back()->setComportement(new CompExplorateur(citoyens.back()));
        }
    }

    fichier.close();

    city = Ville::getVille(citoyens);
    ListeObjets::getInstance();
    attaqueHorde = new AttaqueHorde();
    nombreZombie = attaqueHorde->execute(jour + 1);
}

OutreMonde::~OutreMonde()
{
    for (int i=0;i<taille;i++) {
        for (int j=0;j<taille;j++) {
            delete desert[i][j];
            desert[i][j] = nullptr;
        }
    }

    Ville::killVille();
    ListeObjets::destroy();
    Gauss::destroy();
    delete attaqueHorde;

    sortie.close();
}

Case* OutreMonde::getCase(int x, int y) {
    for (std::vector<Case *> ligne : desert) {
        for (Case *pos : ligne) {
            if (pos->getX() == x && pos->getY() == y) {
                return pos;
            }
        }
    }

    return nullptr;
}

void OutreMonde::proliferation() {

    // Calcul de la nouvelle population
    // Note : actuellement, une case voisine occupée génère un zombie sur la case actuelle
    for(int i=0;i<taille;i++) {
        for (int j=0;j<taille;j++) {
            for (int x=i-i;x<i+2;x++) {
                for (int y=j-1;y<j+2;y++) {
                    if (x>-1 && y>-1 && x<taille && y<taille && (x==i || y==j )
                        && !(i==getCoordVilleX() && j==getCoordVilleY())
                        && desert[x][y]->getZombies()) {
                        float chance =  generateurAleatoire(0,2);
                        bool marchedesMorts = chance < 1;
                        if (marchedesMorts)
                            desert[i][j]->setNewZombies(desert[i][j]->getNewZombies() + 1);
                    }
                }
            }
        }
    }

    // Mise à jour de la population
    for (int i=0;i<taille;i++) {
        for (int j=0;j<taille;j++) {
            Case* trait = desert[i][j];
            trait->setZombies(trait->getNewZombies()+trait->getZombies());
            trait->setNewZombies(0);
        }
    }
}

void OutreMonde::affichageTexte() {
    for (int i=0;i<taille;i++) {
        for (int j=0;j<taille;j++) {
            if (desert[i][j]->getBatiment()) {
                std::cout << "\033[0;31m";
            }

            if (i==getCoordVilleX() && j==getCoordVilleY()) {
                std::cout << "\033[0;34m";
            }

            if (desert[i][j]->getZombies()<10) {
                std::cout << desert[i][j]->getZombies() << "  \033[0m";
            } else {
                std::cout << desert[i][j]->getZombies() << " \033[0m";
            }
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;
}

void OutreMonde::changementJournee() {
    jour++;

    if (jour < 6) {
        multiplicateur = jour;
    }
    else if (jour >= 6 && jour < 15) {
        multiplicateur = jour + 1;
    }
    else if (jour >= 15 && jour < 19) {
        multiplicateur = jour + 4;
    }
    else if (jour >= 19 && jour < 24) {
        multiplicateur = jour + 5;
    }
    else {
        multiplicateur = jour + 6;
    }

    sortie << std::endl << "Jour n°" << jour << std::endl;

    Ville::getVille()->changementJournee();
    Chantiers::getInstance()->changementJournee(true);
    this->gestionAttaqueHorde();
    Chantiers::getInstance()->changementJournee(false);

    this->proliferation();
    this->temps = 0;
}

void OutreMonde::avancerTemps() {
    temps++;

    nuit = temps >= 1140 || temps <= 420;

    if (temps == 1440 || Ville::getVille()->actionsTerminees()) {
        if (!Chantiers::getInstance()->getConstructionParId(ConstructionId::piston)->isConstruit()) {
            for (Citoyen *citoyen : city->getCitoyens()) {
                if (citoyen->getPa() > 0) {
                    citoyen->fermerPortes();
                    break;
                }
            }

            if (Portes::getInstance()->isOuvertes()) {
                for (Citoyen *citoyen : city->getCitoyens()) {
                    if (!citoyen->isRassasie()) {
                        citoyen->prendreNourriture();

                        if (citoyen->getManger()) {
                            citoyen->manger();
                        }
                    }
                    if (citoyen->getPa() == 0 && !citoyen->isDesaltere()) {
                        citoyen->prendreEauBanque();

                        if (citoyen->getBoire()) {
                            citoyen->boire();
                        }
                    }
                    if (citoyen->getPa() == 0) {
                        citoyen->prendreDrogue();

                        if (citoyen->getDrogue()) {
                            citoyen->seDroguer();
                        }
                        else {
                            citoyen->prendreAlcool();
                            citoyen->boireAlcool();
                        }
                    }

                    if (citoyen->getPa() > 0) {
                        citoyen->fermerPortes();
                        Construction *cons = citoyen->getComportement()->choisirConstruction();

                        if (cons != nullptr) {
                            citoyen->construireChantier(cons, citoyen->getPa());
                        }
                        break;
                    }
                }
            }
        }
        changementJournee();
    }

    Portes::getInstance()->setOuvrables(true);
}

void OutreMonde::gestionAttaqueHorde() {
    int citoyensVivants = city->getCitoyens().size();
    int maxZombies = citoyensVivants * multiplicateur;
    int attaques = citoyensVivants;
    std::vector<Citoyen *> ordre = city->getCitoyens();
    std::random_shuffle(ordre.begin(), ordre.end());

    sortie << nombreZombie << " zombies attaquent la ville." << std::endl;

    if (!Portes::getInstance()->isOuvertes()) {
        nombreZombie -= city->getDefenses();
        sortie << city->getDefenses() << " zombies sont retenus par les défenses de la ville." << std::endl;
    }

    int veritableAttaque = std::min(nombreZombie, maxZombies);

    if (nombreZombie - maxZombies > 0) {
        sortie << nombreZombie - maxZombies << " zombies sont vues, errants sans but." << std::endl;
    }

    if (jour < 6) {
        attaques = 0.3 * citoyensVivants;
    }
    else if (!city->getPortes()->isOuvertes()) {
        if (citoyensVivants >= 25) {
            attaques = (rand() % 21 + 40) * citoyensVivants;
        }
    }

    for (int i = 0; i < attaques && veritableAttaque > 0; ++i) {
        Citoyen *citoyen = ordre[i];
        int zombies = 0;

        if (i != attaques - 1) {
            zombies = rand() % (veritableAttaque + 1);
        }
        else {
            zombies = veritableAttaque;
        }


        if (citoyen->getHabitation()->getDefense() < zombies) {
            sortie << citoyen->getNom() << " est dévoré(e) vivant(e) chez lui par " << zombies << " zombies." << std::endl;
            citoyen->mourir(MortId::attaque_ville);
        }
        else {
            if (zombies != 0 && rand() % 100 + 1 <= 30) {
                citoyen->setTerreur(true);
                sortie << citoyen->getNom() << " devient terrorisé(e) après avoir survécu face à " << zombies <<
                " zombies." << std::endl;
            }
            else {
                sortie << citoyen->getNom() << " a survécu(e) face à " << zombies << " zombies sans séquelle." <<
                std::endl;
            }
        }

        veritableAttaque -= zombies;
    }

    nombreZombie = attaqueHorde->execute(jour + 1);
}

// Getter et Setter
const int OutreMonde::getCoordVilleX() {
    return posVilleX;
}

const int OutreMonde::getCoordVilleY() {
    return posVilleY;
}