//
// Created by jozereau on 18/10/16.
//


#include "../../../ressources/outremonde/ville/Habitation.hpp"
#include "../../../ressources/objets/ListeObjets.hpp"

Habitation::Habitation() : defense(1), coffre(listeObjet()) {
    coffre.push_back(ListeObjets::getInstance()->getObjetsParId(ObjetId::doggybag));
    coffre.push_back(ListeObjets::getInstance()->getObjetsParId(ObjetId::affaires));
}

Habitation::~Habitation() {
    for (Objet *objet : coffre) {
        delete objet;
    }
    coffre.clear();
}

bool Habitation::restePlaceCoffre() {
    return (coffre.size() < 4);
}