//
// Created by jozereau on 17/10/16.
//

#include "../../../ressources/outremonde/ville/Citoyen.hpp"
#include "../../../ressources/outremonde/ville/Puits.hpp"
#include "../../../ressources/objets/Conteneur.hpp"
#include "../../../ressources/outremonde/ville/Portes.hpp"
#include "../../../ressources/objets/ListeObjets.hpp"
#include "../../../ressources/outremonde/ville/Banque.hpp"
#include "../../../ressources/outremonde/OutreMonde.hpp"

Citoyen::Citoyen(int id, std::string nom) :
        id(id), nom(nom), comportement(nullptr), posX(0), posY(0), enVille(true), paMax(6), pa(6),
        inventaire(listeObjet()), capacite(4), ration(0), banni(false), blessures(std::vector<Blessure>()),
        eau(Eau::aucun), desaltere(false), alcool(Alcool::aucun), rassasie(false), clair(true), drogue(false),
        dependant(false), campeurAvise(false), terreur(false), immunise(false), infecte(false), convalescense(false),
        habitation(new Habitation()) {
}

Citoyen::~Citoyen() {
    inventaire.clear();

    delete habitation;
    habitation = nullptr;

    if (comportement != nullptr) {
        delete comportement;
        comportement = nullptr;
    }

}

// Méthodes privées
    // Gestion des états
void Citoyen::remettrePa(int pa_bonus) {
    this->pa = paMax + pa_bonus;
}

    // Gestion des objets
void Citoyen::ramasserObjet(Objet *objet) {
    if (objet != nullptr) {
        if (this->restePlaceInventaire()) {
            inventaire.push_back(objet);
        }
        else {
            OutreMonde::sortie << "Inventaire de " << this->nom << " est plein.\nIl ne peut ramasser "
            << objet->getNom() << std::endl;
        }
    }
}
void Citoyen::prendreBanque(ObjetId objet) {
    if (this->restePlaceInventaire() && Banque::getInstance()->getNbObjets(objet) > 0) {
        Objet *obj = ListeObjets::getInstance()->getObjetsParId(objet);

        OutreMonde::sortie << this->nom << " prends " << obj->getNom() << " de la banque." << std::endl;
        this->ramasserObjet(obj);
        Banque::getInstance()->enleverObjet(objet);
    }
}
void Citoyen::libererInventaire(Objet *objet) {
    if (objet != nullptr && objet->detruireObjet()) {
        for (listeObjet::iterator i = this->inventaire.begin(); i != this->inventaire.end(); ++i) {
            if (*i == objet) {
                *i = nullptr;
                this->inventaire.erase(i);
                delete objet;
                break;
            }
        }
    }
}
void Citoyen::consommer(Consommable *consommable) {
    bool consomme = true;

    if (consommable->isAlcool()) {
        if (this->alcool != Alcool::gueuleDeBois) {
            this->alcool = Alcool::ivre;
            this->remettrePa(consommable->getPa_bonus());
        }
        else {
            OutreMonde::sortie << this->nom << " ne peut pas boire d'alcool aujourd'hui." << std::endl;
            consomme = false;
        }
    }

    if (consommable->isDrogue()) {
        if (this->drogue) {
            this->dependant = true;
        }

        if (!this->dependant) {
            this->dependant = consommable->isDependance();
        }

        if (consommable->isPa_rendu()) {
            this->remettrePa(consommable->getPa_bonus());
        }
        this->drogue = true;
        this->clair = false;
    }

    if (consommable->isEau()) {
        if (!this->desaltere) {
            this->desaltere = true;

            if (this->eau == Eau::aucun || this->eau == Eau::soif) {
                this->eau = Eau::aucun;
                this->remettrePa(consommable->getPa_bonus());
            }
            else if (this->eau == Eau::deshydrate) {
                this->eau = Eau::soif;
            }
        }
        else {
            consomme = false;
            OutreMonde::sortie << this->nom << " ne peut pas boire plus aujourd'hui." << std::endl;
        }
    }

    if (consommable->isNourriture()) {
        if (!this->rassasie) {
            this->rassasie = true;
            this->remettrePa(consommable->getPa_bonus());
        }
        else {
            consomme = false;
            OutreMonde::sortie << this->nom << " ne peut pas manger plus aujourd'hui." << std::endl;
        }
    }

    if (consommable->isTerreur()) {
        this->terreur = true;
        OutreMonde::sortie << this->nom << " est terrorisé." << std::endl;
    }

    if (consommable->isInfection()) {
        this->infecte = true;
        OutreMonde::sortie << this->nom << " est infecté." << std::endl;
    }

    if (consommable->isSoin()) {
        if ((!this->convalescense && !this->blessures.empty())) {
            this->blessures.erase(this->blessures.begin() + rand() % this->blessures.size());
        }
        else if (this->convalescense){
            consomme = false;
            OutreMonde::sortie << this->nom << " ne peut pas être soigné plus aujourd'hui." << std::endl;
        }
        else {
            consomme = false;
            OutreMonde::sortie << this->nom << " n'est pas blessé." << std::endl;
        }
    }

    if (consomme) {
        OutreMonde::sortie << nom << " consomme " << consommable->getNom() << std::endl;
        libererInventaire(consommable);
    }
}
void Citoyen::mettreObjetsBanque() {
    for (Objet *objet : inventaire) {
        poserBanque(objet, true);
    }
}

    // Gestion des déplacements
void Citoyen::seDeplacer(Direction direction) {
        switch (direction) {
            case Direction::nord :
                posY--;
                OutreMonde::sortie << this->nom << " se déplace vers le nord. (" << posX << ", " << posY << ")" << std::endl;
                break;
            case Direction ::sud :
                posY++;
                OutreMonde::sortie << this->nom << " se déplace vers le sud. (" << posX << ", " << posY << ")" << std::endl;
                break;
            case Direction::ouest :
                posX--;
                OutreMonde::sortie << this->nom << " se déplace vers l'ouest. (" << posX << ", " << posY << ")" << std::endl;
                break;
            case Direction::est :
                posX++;
                OutreMonde::sortie << this->nom << " se déplace vers l'est. (" << posX << ", " << posY << ")" << std::endl;
                break;
        }
    }

// Fin méthodes privées

// Méthodes publiques
    // Méthodes globales
void Citoyen::execute() {
    if (comportement != nullptr) {
        comportement->execute();
    }
}
bool Citoyen::changementJournee() {
    remettrePa(0);

    if (comportement != nullptr) {
        comportement->reinitialiser();
    }
    ration = 0;

    if (!enVille) {
        mourir(MortId::attaque_desert);
        return true;
    }

    if (!desaltere) {
        switch (eau) {
            case Eau::aucun :
                eau = Eau::soif;
                break;
            case Eau::soif :
                eau = Eau::deshydrate;
                break;
            case Eau::deshydrate :
                mourir(MortId::deshydratation);
                return true;
        }
    }

    if (infecte && rand() % 100 + 1 > 50) {
        mourir(MortId::infection);
        return true;
    }

    if (dependant && !drogue) {
        mourir(MortId::dependance);
        return true;
    }

    drogue = false;
    desaltere = false;
    rassasie = false;

    return false;
}


    // Gestion de l'inventaire
bool Citoyen::restePlaceInventaire() {
    return (inventaire.size() < capacite);
}
Objet* Citoyen::getInventaire(ObjetId objet) {
    listeObjet::iterator i;
    int quantite = 0;

    for (i = inventaire.begin(); i != inventaire.end() && quantite < capacite; ++i, ++quantite) {
        if ((*i)->getId() == objet) {
            return *i;
        }
    }
    return nullptr;
}

bool Citoyen::getManger() {
    for (Objet *objet : this->inventaire) {
        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) objet;

            if (consommable->isNourriture()) {
                return true;
            }
        }
    }

    return false;
}
bool Citoyen::getBoire() {
    return getInventaire(ObjetId::ration) != nullptr;
}
bool Citoyen::getArme() {
    for (Objet *objet : this->inventaire) {
        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::arme) {
            return true;
        }
    }
    return false;
}
bool Citoyen::getDrogue() {
    for (Objet *objet : this->inventaire) {
        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) objet;

            if (consommable->isDrogue()) {
                return true;
            }
        }
    }

    return false;
}

    // Gestion des objets
void Citoyen::utiliserObjet(ObjetId objet) {
    Objet *obj = nullptr;
    Consommable *consommable = nullptr;
    Conteneur *conteneur = nullptr;
    Arme *arme = nullptr;

    if ((obj = this->getInventaire(objet)) != nullptr) {
        TypeObjet type = obj->utiliser(this->inventaire);

        switch (type) {
            case TypeObjet::objet :
                break;

            case TypeObjet::consommable :
                consommable = (Consommable *) obj;

                this->consommer(consommable);
                break;

            case TypeObjet::conteneur :
                conteneur = (Conteneur *) obj;

                this->ramasserObjet(conteneur->getDrop());
                libererInventaire(obj);
                break;

            case TypeObjet::arme :
                break;
        }
    }
}

void Citoyen::poserBanque(Objet *objet, bool mort) {
    for (listeObjet::iterator i = inventaire.begin(); i != inventaire.end(); ++i) {
        if (*i == objet) {
            if (!mort) {
                OutreMonde::sortie << this->nom << " pose " << objet->getNom() << " dans la banque." << std::endl;
            }

            Banque::getInstance()->ajouterObjet(objet->getId());
            delete objet;
            *i = nullptr;
            inventaire.erase(i);
            break;
        }
    }
}
void Citoyen::prendreCoffre(ObjetId objet) {
    if (this->restePlaceInventaire()) {
        for (Objet *obj : this->getHabitation()->getCoffre()) {
            if (obj->getId() == objet) {
                this->ramasserObjet(ListeObjets::getInstance()->getObjetsParId(objet));
                OutreMonde::sortie << this->nom << " récupère " << obj->getNom() << " de son coffre." << std::endl;
                break;
            }
        }
    }
}
void Citoyen::poserCoffre(Objet *objet) {
    if (habitation->restePlaceCoffre()) {
        transfertObjet(inventaire, habitation->getCoffre(), objet);
        OutreMonde::sortie << this->nom << " dépose " << objet->getNom() << " dans son coffre." << std::endl;
    }
}

void Citoyen::prendreObjetRare() {
    Case *pos = OutreMonde::getInstance()->getCase(posX, posY);

    for (Objet *obj : pos->getObjetAuSol()) {
        for (Objet *inv : inventaire) {
            if (plusRare(obj, inv)) {
                this->poserObjetDehors(inv);
                this->ramasserObjetDehors(obj);
                break;
            }
        }
    }
}

void Citoyen::prendreRation() {
    if (isEnVille()) {
        if (Puits::getInstance()->getQuantite() != 0 && this->restePlaceInventaire()) {
            switch (ration) {
                case 0:
                    ration++;
                    ramasserObjet(Puits::getInstance()->prendreRation());
                    OutreMonde::sortie << this->nom << " se sert dans le puits." << std::endl;
                    break;
                case 1:
                    if (Puits::getInstance()->isPompe()) {
                        ration++;
                        ramasserObjet(Puits::getInstance()->prendreRation());
                        OutreMonde::sortie << this->nom << " se sert dans le puits une deuxieme fois." << std::endl;
                    }
                    else {
                        OutreMonde::sortie << this->nom <<
                        " ne peut pas se servir dans le puits une deuxieme fois sans la pompe." << std::endl;
                    }
                    break;
                default:
                    OutreMonde::sortie << this->nom << " ne peut plus prendre de ration d'eau." << std::endl;
                    break;
            }
        }
    }
}
void Citoyen::prendreEauBanque() {
    if (Banque::getInstance()->getNbObjets(ObjetId::ration) > 0) {
        prendreBanque(ObjetId::ration);
    }
    else {
        Objet *objet = nullptr;

        for (listeBanque::iterator i = Banque::getInstance()->getObjets().begin();
             i != Banque::getInstance()->getObjets().end(); ++i) {
            objet =ListeObjets::getInstance()->getObjetsParId((*i).first);

            if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
                Consommable *consommable = (Consommable *) objet;

                if (consommable->isEau()) {
                    prendreBanque(consommable->getId());
                    break;
                }
            }

            delete objet;
        }

        delete objet;
    }
}
void Citoyen::prendreNourriture() {
    listeBanque::iterator i;
    Consommable *autreBouffe = nullptr;
    bool trouve = false;

    for (i = Banque::getInstance()->getObjets().begin(); i != Banque::getInstance()->getObjets().end(); ++i) {
        Objet *objet =ListeObjets::getInstance()->getObjetsParId((*i).first);

        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) objet;

            if (consommable->isNourriture()) {
                if (consommable->getPa_bonus() == 0) {
                    trouve = true;
                    prendreBanque(consommable->getId());

                    delete objet;
                    break;
                }
                else {
                    delete objet;
                }
            }
            else {
                if (autreBouffe != nullptr) {
                    delete autreBouffe;
                }
                autreBouffe = consommable;
            }
        }
        else {
            delete objet;
        }
    }

    if (autreBouffe != nullptr) {
        if (!trouve) {
            prendreBanque(autreBouffe->getId());
        }

        delete autreBouffe;
    }
}
void Citoyen::prendreDrogue() {
    listeBanque::iterator i;
    Consommable *autreDrogue = nullptr;
    bool trouve = false;

    for (i = Banque::getInstance()->getObjets().begin(); i != Banque::getInstance()->getObjets().end(); ++i) {
        Objet *objet =ListeObjets::getInstance()->getObjetsParId((*i).first);

        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) objet;

            if (consommable->isDrogue()) {
                if (!consommable->isAleatoire()) {
                    trouve = true;
                    prendreBanque(consommable->getId());

                    delete objet;
                    break;
                }
                else {
                    delete objet;
                }
            }
            else {
                if (autreDrogue != nullptr) {
                    delete autreDrogue;
                }
                autreDrogue = consommable;
            }
        }
        else {
            delete objet;
        }
    }

    if (autreDrogue != nullptr) {
        if (!trouve) {
            prendreBanque(autreDrogue->getId());
        }

        delete autreDrogue;
    }
}
void Citoyen::prendreAlcool() {
    listeBanque::iterator i;

    for (i = Banque::getInstance()->getObjets().begin(); i != Banque::getInstance()->getObjets().end(); ++i) {
        Objet *objet =ListeObjets::getInstance()->getObjetsParId((*i).first);

        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) objet;

            if (consommable->isAlcool()) {
                prendreBanque(consommable->getId());

                delete objet;
                break;
            }
        }
        else {
            delete objet;
        }
    }
}
void Citoyen::prendreArme() {
    listeBanque::iterator i;

    for (i = Banque::getInstance()->getObjets().begin(); i != Banque::getInstance()->getObjets().end(); ++i) {
        Objet *objet = ListeObjets::getInstance()->getObjetsParId((*i).first);

        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::arme) {
            Arme *arme = (Arme *) objet;

            if (arme->isCasse()) {
                delete objet;
                continue;
            }
            else if (arme->isRecharge() && arme->isVide()) {
                if (Banque::getInstance()->getNbObjets(arme->getMunitions()) > 0) {
                    this->prendreBanque(arme->getId());
                    OutreMonde::sortie << nom << " recharge " << arme->getNom() << std::endl;
                    arme->recharger();
                    Banque::getInstance()->enleverObjet(arme->getMunitions());

                    delete objet;
                    break;
                }
                else {
                    delete objet;
                    continue;
                }
            }
            else {
                this->prendreBanque(arme->getId());
                delete objet;
                break;
            }
        }
        else {
            delete objet;
        }
    }
}

    // Utilisation des objets
void Citoyen::manger() {
    if (!this->rassasie) {
        for (Objet *objet : this->inventaire) {
            if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
                Consommable *consommable = (Consommable *) objet;

                if (consommable->isNourriture()) {
                    this->consommer(consommable);
                    break;
                }
            }
        }
    }
}
void Citoyen::boire() {
    if (!this->isDesaltere() && this->getInventaire(ObjetId::ration) != nullptr) {
        this->utiliserObjet(ObjetId::ration);
    }
}
void Citoyen::seDroguer() {
    for (Objet *objet : this->inventaire) {
        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) objet;

            if (consommable->isDrogue()) {
                this->consommer(consommable);
                break;
            }
        }
    }
}
void Citoyen::boireAlcool() {
    for (Objet *objet : this->inventaire) {
        if (objet != nullptr && objet->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) objet;

            if (consommable->isAlcool()) {
                this->consommer(consommable);
                break;
            }
        }
    }
}
void Citoyen::attaquer() {
    for (Objet *obj : inventaire) {
        if (obj->getTypeObjet() == TypeObjet::arme) {
            Arme *arme = (Arme *) obj;

            if (arme->isRecharge()) {
                if (arme->isVide()) {
                    if (getInventaire(arme->getMunitions()) != nullptr) {
                        OutreMonde::sortie << nom << " recharge " << arme->getNom() << std::endl;
                        arme->recharger();
                        libererInventaire(getInventaire(arme->getMunitions()));
                        break;
                    }
                    else {
                        this->poserObjetDehors(arme);
                        break;
                    }
                }
                else {
                    int nbTues = arme->nombreTues();
                    OutreMonde::sortie << nom << " utilise " << arme->getNom() << " et tue " << nbTues << " zombie(s)." <<
                    std::endl;
                    OutreMonde::getInstance()->getCase(posX, posY)->tuerZombie(nbTues);
                }
            }
            else {
                if (!arme->isCasse()) {
                    int nbTues = arme->nombreTues();
                    OutreMonde::sortie << nom << " utilise " << arme->getNom() << " et tue " << nbTues << " zombie(s)." <<
                    std::endl;
                    OutreMonde::getInstance()->getCase(posX, posY)->tuerZombie(nbTues);

                    if (arme->getDestructibilite() == 100) {
                        libererInventaire(arme);
                    }
                    else if (!arme->isRecharge() && arme->detruireObjet()) {
                        OutreMonde::sortie << arme->getNom() << " casse." << std::endl;
                        arme->setCasse(true);
                        this->poserObjetDehors(arme);

                    }
                    break;
                }
                else {
                    this->poserObjetDehors(arme);
                    break;
                }
            }
        }
    }
}

    // Gestion de l'extérieur
void Citoyen::ramasserObjetDehors(Objet *objet) {
    if (objet != nullptr) {
        OutreMonde::sortie << this->nom << " ramasse " << objet->getNom() << " sur le sol." << std::endl;
        this->ramasserObjet(objet);
        OutreMonde::getInstance()->getCase(posX, posY)->ramasserObjet(objet);
    }
}
void Citoyen::poserObjetDehors(Objet *objet) {
    if (objet != nullptr) {
        OutreMonde::sortie << nom << " pose " << objet->getNom() << " sur le sol." << std::endl;
        OutreMonde::getInstance()->getCase(posX, posY)->deposerObjet(objet);

        for (listeObjet::iterator i = inventaire.begin(); i != inventaire.end(); ++i) {
            if (*i == objet) {
                *i = nullptr;
                inventaire.erase(i);
                break;
            }
        }
    }
}
void Citoyen::fouiller() {
    if ((posX != 0 || posY != 0) && rand() % 100 + 1 <= 60) {
        Objet *drop = OutreMonde::getInstance()->getCase(posX, posY)->fouille();

        if (drop != nullptr) {
            if (restePlaceInventaire()) {
                OutreMonde::sortie << this->nom << " trouve " << drop->getNom() << " en fouillant." << std::endl;
                ramasserObjet(drop);
            }
            else {
                OutreMonde::sortie << this->nom << " dépose " << drop->getNom() << " sur le sol. (Inventaire plein)" <<
                std::endl;
                OutreMonde::getInstance()->getCase(posX, posY)->deposerObjet(drop);
            }
        }
    }
}


    // Gestion des déplacements
void Citoyen::ouvrirPortes() {
    if (pa > 0 && enVille && !Portes::getInstance()->isDetruites() && Portes::getInstance()->isOuvrables() && !Portes::getInstance()->isOuvertes()) {
        Portes::getInstance()->setOuvertes(true);
        --pa;
        OutreMonde::sortie << this->nom << " ouvre les portes de la ville." << std::endl;
    }
}
void Citoyen::fermerPortes() {
    if (pa > 0 && enVille && !Portes::getInstance()->isDetruites() && Portes::getInstance()->isOuvertes()) {
        Portes::getInstance()->setOuvertes(false);
        --pa;
        OutreMonde::sortie << this->nom << " ferme les portes de la ville." << std::endl;
    }
}
void Citoyen::sortir() {
    if (enVille && (Portes::getInstance()->isOuvertes() || Portes::getInstance()->isDetruites())) {
        OutreMonde::sortie << this->nom << " sort explorer l'Outre-Monde." << std::endl;
        enVille = false;
    }
}
void Citoyen::entrer() {
    if (!enVille && (Portes::getInstance()->getInstance()->isOuvertes() || Portes::getInstance()->isDetruites())) {
        OutreMonde::sortie << this->nom << " revient de l'Outre-Monde." << std::endl;
        enVille = true;
    }
}
void Citoyen::seDeplacer(Case *position) {
    if (position != nullptr) {
        if (this->pa > 0) {
            --pa;
            int x = this->posX - position->getX();
            int y = this->posY - position->getY();

            OutreMonde::getInstance()->getCase(posX, posY)->changerPointControle(-2);
            position->changerPointControle(2);

            if (x == 0 && y == -1) {
                this->seDeplacer(Direction::sud);
            }
            if (x == 0 && y == 1) {
                this->seDeplacer(Direction::nord);
            }
            if (x == -1 && y == 0) {
                this->seDeplacer(Direction::est);
            }
            if (x == 1 && y == 0) {
                this->seDeplacer(Direction::ouest);
            }
        }
    }
}

    // Gestion des chantiers
void Citoyen::construireChantier(Construction *cons, int pa) {

    if (pa > 0) {
        OutreMonde::sortie << nom << " travaille sur le chantier " << cons->getNom() << std::endl;
        this->pa = cons->enConstruction(std::min(pa, this->pa));
    }
}
bool Citoyen::transformerRessources(ObjetId ressource) {
    if (Chantiers::getInstance()->getAtelier()->isConstruit()) {
        int cout = 3;

        if (Chantiers::getInstance()->getConstructionParId(ConstructionId::manufacture)->isConstruit()) {
            cout--;
        }

        if (getInventaire(ObjetId::scie) != nullptr) {
            cout--;
        }
        else if (Banque::getInstance()->getNbObjets(ObjetId::scie) > 0) {
            cout--;
        }

        if (this->pa >= cout) {
            this->pa -= cout;

            if (getInventaire(ObjetId::scie) == nullptr && Banque::getInstance()->getNbObjets(ObjetId::scie) > 0) {
                prendreBanque(ObjetId::scie);
            }

            OutreMonde::sortie << nom << " travaille à l'atelier." << std::endl;
            Chantiers::getInstance()->getAtelier()->transformer(ressource);

            if (getInventaire(ObjetId::scie) != nullptr) {
                poserBanque(getInventaire(ObjetId::scie));
            }

            return true;
        }
    }

    return false;
}

    // Gestion de la mort
void Citoyen::mourir(MortId mort) {
    std::string typeMort;

    switch (mort) {
        case MortId::dependance :
            typeMort = " de dépendance. La drogue n'est pas sans conséquences.";
            break;
        case MortId::deshydratation :
            typeMort = " de déshydratation. L'eau c'est important vous savez.";
            break;
        case MortId::infection :
            typeMort = " d'infection. Apprenez à vous soigner.";
            break;
        case MortId::attaque_desert :
            typeMort = " dans le désert. Tenté mais perdu.";
            break;
        default :
            break;
    }

    if (mort != MortId::attaque_ville) {
        OutreMonde::sortie << this->getNom() << " est mort" << typeMort << std::endl;
    }


    if (posX == 0 && posY == 0) {
        mettreObjetsBanque();
    }
    else {
        Case *position = OutreMonde::getInstance()->getCase(posX, posY);

        for (Objet *objet : inventaire) {
            position->deposerObjet(objet);
        }
        position->deposerObjet(ListeObjets::getInstance()->getObjetsParId(ObjetId::os_charnu));
    }

    Ville::getVille()->citoyenMort(id);
}

// Fin méthodes publiques