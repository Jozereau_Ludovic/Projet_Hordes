//
// Created by jozereau on 10/02/17.
//

#include "../../../../ressources/outremonde/ville/chantiers/Atelier.hpp"
#include "../../../../ressources/outremonde/ville/chantiers/Chantiers.hpp"
#include "../../../../ressources/outremonde/ville/Banque.hpp"

Atelier::Atelier() : Construction(ConstructionId::atelier, "Atelier", 0, 25, false,
                                  {{ObjetId::planche, 10}, {ObjetId::ferraille, 8}}), schemas(listeConversion()) {

    // Bois
    schemas.push_back({{ObjetId::planche}, ObjetId::souche});
    schemas.push_back({{ObjetId::planche}, ObjetId::poutre});
    schemas.push_back({{ObjetId::poutre}, ObjetId::planche});

    // Métal
    schemas.push_back({{ObjetId::ferraille}, ObjetId::debris});
    schemas.push_back({{ObjetId::ferraille}, ObjetId::structure});
    schemas.push_back({{ObjetId::structure}, ObjetId::ferraille});

    // Meuble en kit
    schemas.push_back({{ObjetId::chaise, ObjetId::rocking, ObjetId::table, ObjetId::porte, ObjetId::treteau},
                      ObjetId::meuble_kit});

    // Mecanisme
    schemas.push_back({{ObjetId::tube}, ObjetId::mecanisme});
}

Atelier::~Atelier() {
    schemas.clear();
}

listeConversion Atelier::ressourcesTransformables() { // Liste des ressources transformables pour l'atelier
    listeConversion ressources;

    for (std::pair<std::vector<ObjetId>, ObjetId> schema : schemas) {
        if (Banque::getInstance()->getNbObjets(schema.second) > 0) {
            ressources.push_back(schema);
        }
    }

    return ressources;
}

std::vector<ObjetId> Atelier::ressourcesUtiles(Construction *cons) { // Liste des ressources utiles pour une construction
    listeConversion ressources = ressourcesTransformables();
    std::vector<ObjetId> utiles;

    for (std::pair<std::vector<ObjetId>, ObjetId> ressource : ressources) {
        for (std::pair<ObjetId, int> utile : cons->getMateriaux()) {
            for (ObjetId destination : ressource.first) {
                if (Banque::getInstance()->getNbObjets(utile.first) < utile.second && utile.first == destination) {
                    utiles.push_back(ressource.second);

                    break;
                }
            }
        }
    }

    return utiles;
}

void Atelier::transformer(ObjetId ressource) {
    if (Banque::getInstance()->getNbObjets(ressource) > 0) {
        for (std::pair<std::vector<ObjetId>, ObjetId> schema : schemas) {
            if (schema.second == ressource) {
                Banque::getInstance()->ajouterObjet(schema.first[rand() % schema.first.size()]);
                Banque::getInstance()->enleverObjet(ressource);

                break;
            }
        }
    }
}

void Atelier::updateCouts() {
    for (Construction *cons : Chantiers::getInstance()->getConstructions()) {
        cons->setPaRestants(cons->getPaRestants() * (1 - reductionPa/100));
        cons->setPaTotaux(cons->getPaTotaux() * (1 - reductionPa/100));
    }
}