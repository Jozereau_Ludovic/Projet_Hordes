//
// Created by jozereau on 09/12/16.
//



#include "../../../../ressources/outremonde/ville/chantiers/Construction.hpp"
#include "../../../../ressources/outremonde/ville/Banque.hpp"
#include "../../../../ressources/outremonde/ville/Ville.hpp"
#include "../../../../ressources/objets/ListeObjets.hpp"
#include "../../../../ressources/outremonde/OutreMonde.hpp"

Construction::Construction(ConstructionId id, std::string nom, int defense, int paTotaux, bool temporaire, listeMateriaux materiaux,
                            listePrerequis prerequis, bool evolutif, int niveauMax) :
        id(id), nom(nom), defense(defense), paTotaux(paTotaux), resistance(paTotaux), temporaire(temporaire), construit(false), materiaux(materiaux),
        prerequis(prerequis), paRestants(paTotaux), constructible(false), evolutif(evolutif), niveau(0), niveauMax(niveauMax) {
}
Construction::~Construction() {
    materiaux.clear();
    prerequis.clear();
}

// Méthodes privées
void Construction::evolutionPompe() {
    int eauSuppl = 0;
    switch (niveau) {
        case 1:
        case 2:
            eauSuppl = 20;
            break;
        case 3:
        case 4:
            eauSuppl = 30;
            break;
        case 5:
            eauSuppl = 40;
            break;
        default:
            break;
    }

    if (eauSuppl > 0) {
        OutreMonde::sortie << "Ajout de " << eauSuppl << " rations dans le puits." << std::endl;
        Puits::getInstance()->setQuantite(Puits::getInstance()->getQuantite() + eauSuppl);
    }
}
void Construction::evolutionGrandFosse() {
    switch (niveau) {
        case 1:
            this->defense += 13;
            Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() + 13);
            break;
        case 2:
            this->defense += 21;
            Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() + 21);
            break;
        case 3:
            this->defense += 32;
            Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() + 32);
            break;
        case 4:
            this->defense += 33;
            Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() + 33);
            break;
        case 5:
            this->defense += 51;
            Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() + 51);
            break;
        default:
            break;
    }
}
void Construction::evolutionAquaTourelles() {
    switch (niveau) {
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            break;
        default:
            break;
    }
}

// Fin méthodes privées

std::vector<Construction *> Construction::getPrerequisNonConstruits() {
    std::vector<Construction *> prerequisCons;

    for (ConstructionId id : prerequis) {
        Construction *cons = Chantiers::getInstance()->getConstructionParId(id);

        if (cons != nullptr && !cons->isConstruit()) {
            prerequisCons.push_back(cons);
        }
    }

    return prerequisCons;
}

std::vector<Construction *> Construction::getPrerequisConstructibles() {
    std::vector<Construction *> prerequisCons;

    for (ConstructionId id : prerequis) {
        Construction *cons = Chantiers::getInstance()->getConstructionParId(id);

        if (cons != nullptr && cons->estConstructible()) {
            prerequisCons.push_back(cons);
        }
    }

    return prerequisCons;
}

/** Pour chaque matériau requis, on regarde en banque si la quantité est suffisante **/
bool Construction::estConstructible() {
    bool matSuff = true;

    for (auto i = materiaux.begin(); i != materiaux.end(); ++i) {
        if (Banque::getInstance()->getNbObjets(i->first) < i->second) {
            matSuff = false;
            break;
        }
    }

    if (matSuff) {
        for (ConstructionId id : prerequis) {
            Construction *cons = Chantiers::getInstance()->getConstructionParId(id);

            if (cons != nullptr && !cons->construit) {
                matSuff = false;
                break;
            }
        }
    }

    if (construit) {
        matSuff = false;
    }

    constructible = matSuff;
    return constructible;
}

int Construction::enConstruction(int pa) {
    if (estConstructible() && !construit) {
        paRestants -= pa;
        pa = 0;

        if (paRestants < 0) {
            pa = -paRestants;
            paRestants = 0;

            this->estConstruit();
        }
    }
    return pa;
}

void Construction::estConstruit() {
    construit = true;

    for (auto i = materiaux.begin(); i != materiaux.end(); ++i) {
        Banque::getInstance()->setNbObjets(i->first, Banque::getInstance()->getNbObjets(i->first) - i->second);
    }

    OutreMonde::sortie << "Construction de " << nom << "." << std::endl;

    Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() + this->defense);
}

float Construction::getCoutTotal() {
    float cout = this->paRestants;

    for (std::pair<ObjetId, int> materiel : materiaux) {
        Objet *obj = ListeObjets::getInstance()->getObjetsParId(materiel.first);

        cout += (int) obj->getRarete() * materiel.second;

        delete obj;
    }

    return cout;
}

int Construction::getCoutRation() {
    for (std::pair<ObjetId, int> materiel : materiaux) {
        if (materiel.first == ObjetId::ration) {
            return materiel.second;
        }
    }

    return 0;
}

void Construction::detruire() {
    constructible = estConstructible();
    construit = false;
    paRestants = paTotaux;

    Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() - this->defense);
}

bool Construction::isNiveauMax() {
    return niveau == niveauMax;
}

void Construction::evoluer() {
    if (!isNiveauMax()) {
        OutreMonde::sortie << "Evolution de " << nom << std::endl;
        niveau++;

        switch (id) {
            case ConstructionId::pompe :
                evolutionPompe();
                break;

            case ConstructionId::atelier :
                Chantiers::getInstance()->getAtelier()->setReductionPa(Chantiers::getInstance()->getAtelier()->getReductionPa() + 5);
                Chantiers::getInstance()->getAtelier()->updateCouts();
                break;

            case ConstructionId::grand_fosse :
                evolutionGrandFosse();
                break;

            case ConstructionId::aqua_tourelles :
                evolutionAquaTourelles();
                break;

            default:
                break;
        }
    }
}