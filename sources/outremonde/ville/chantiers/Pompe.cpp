//
// Created by jozereau on 02/03/17.
//

#include "../../../../ressources/outremonde/ville/chantiers/Pompe.hpp"
#include "../../../../ressources/outremonde/ville/Puits.hpp"
#include "../../../../ressources/outremonde/OutreMonde.hpp"

Pompe::Pompe(ConstructionId id, std::string nom, int eauSuppl, int paTotaux, bool temporaire,
             listeMateriaux materiaux, listePrerequis prerequis, bool evolutif) :
        Construction(id, nom, 0, paTotaux, temporaire, materiaux, prerequis, evolutif), eauSuppl(eauSuppl) {
}

void Pompe::estConstruit() {
    Construction::estConstruit();

    OutreMonde::sortie << "Ajout de " << eauSuppl << " dans le puits." << std::endl;
    Puits::getInstance()->setQuantite(Puits::getInstance()->getQuantite() + this->eauSuppl);
}