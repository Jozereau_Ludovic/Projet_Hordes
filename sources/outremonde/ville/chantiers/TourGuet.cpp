//
// Created by jozereau on 06/03/17.
//

#include "../../../../ressources/outremonde/ville/chantiers/TourGuet.hpp"
#include "../../../../ressources/outremonde/OutreMonde.hpp"

TourGuet::TourGuet() :
        Construction(ConstructionId::tour_guet, "Tour de guet", 0, 12, false,
                     {{ObjetId::planche, 3}, {ObjetId::ferraille, 2}}), precision(0), incertitude(0), previsionMin(0),
        previsionMax(0), coefficient(0) { }

void TourGuet::execute() {
    int attaque = OutreMonde::getInstance()->getNombreZombie();

    coefficient = 0.15 / precision;
    incertitude = attaque * coefficient;
    previsionMin = attaque - rand()%(incertitude/2 + 1);
    previsionMax = previsionMin + incertitude;
}

void TourGuet::scruter() {
    precision = std::min(precision + 0.05, 1.0);
    execute();
}

void TourGuet::reinitialiser() {
    precision = 0;
}