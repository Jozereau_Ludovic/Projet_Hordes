//
// Created by jozereau on 09/12/16.
//

#include "../../../../ressources/outremonde/ville/chantiers/Chantiers.hpp"
#include "../../../../ressources/outremonde/ville/chantiers/Pompe.hpp"
#include "../../../../ressources/outremonde/ville/Ville.hpp"
#include "../../../../ressources/outremonde/OutreMonde.hpp"

Chantiers::Chantiers() : recommande(nullptr) {
    // Branche atelier
    constructions.push_back(new Atelier());
    atelier = (Atelier *) constructions.back();
    constructions.push_back(new Construction(ConstructionId::tourniquet, "Tourniquet", 10, 15, false,
                                             {{ObjetId::poutre, 2}, {ObjetId::structure, 1}}, {ConstructionId::atelier}));
    constructions.push_back(new Construction(ConstructionId::manufacture, "Manufacture", 0, 40, false,
                                             {{ObjetId::poutre, 5}, {ObjetId::structure, 5}, {ObjetId::table, 1}}, {ConstructionId::atelier}));
    constructions.push_back(new Construction(ConstructionId::scies, "Scies hurlantes", 45, 65, false,
                                             {{ObjetId::ferraille, 5}, {ObjetId::structure, 2}, {ObjetId::pve, 3}, {ObjetId::rustine, 3}}, {ConstructionId::atelier}));
    constructions.push_back(new Construction(ConstructionId::supports_def, "Supports défensifs", 8, 55, false,
                                             {{ObjetId::poutre, 5}, {ObjetId::structure, 10}}, {ConstructionId::atelier}, true, 2));
    constructions.push_back(new Construction(ConstructionId::cimetiere, "Cimetière cadenassé", 0, 36, false,
                                             {{ObjetId::poutre, 2}, {ObjetId::structure, 1}}, {ConstructionId::atelier}));

    // Instanciation tour de guet
    constructions.push_back(new TourGuet());
    tourGuet = (TourGuet *) constructions.back();

    // Instanciation pompe
    constructions.push_back(new Pompe(ConstructionId::pompe, "Pompe", 5, 25, false,
                                             {{ObjetId::ferraille, 8}, {ObjetId::tube, 1}}, listePrerequis(), true));
    constructions.push_back(new Construction(ConstructionId::potager, "Potager", 0, 60, false,
                                             {{ObjetId::poutre, 10}, {ObjetId::ration, 10}, {ObjetId::produits, 1}}, {ConstructionId::pompe}));
    constructions.push_back(new Construction(ConstructionId::pampl_explo, "Pamplemousses explosifs", 0, 30, false,
                                             {{ObjetId::planche, 5}, {ObjetId::ration, 10}, {ObjetId::explosifs, 5}}, {ConstructionId::potager}));
    constructions.push_back(new Construction(ConstructionId::fertilisation, "Fertilisation sauvage", 0, 30, false,
                                             {{ObjetId::ferraille, 5}, {ObjetId::ration, 10}, {ObjetId::produits, 8}, {ObjetId::steroides, 2}}, {ConstructionId::potager}));
    constructions.push_back(new Pompe(ConstructionId::foreuse, "Foreuse puits", 40, 60, false,
                                             {{ObjetId::poutre, 7}, {ObjetId::structure, 2}}, {ConstructionId::pompe}));
    constructions.push_back(new Pompe(ConstructionId::eden, "Projet Eden", 70, 65, false,
                                             {{ObjetId::poutre, 5}, {ObjetId::structure, 8}, {ObjetId::explosifs, 2}}, {ConstructionId::foreuse}));
    constructions.push_back(new Pompe(ConstructionId::reseau, "Réseau hydrolique", 15, 40, false,
                                             {{ObjetId::ferraille, 5}, {ObjetId::structure, 5}, {ObjetId::pve, 3}, {ObjetId::tube, 2}}, {ConstructionId::pompe}));
    constructions.push_back(new Construction(ConstructionId::vaporisateur, "Vaporisateur", 50, 50, false,
                                             {{ObjetId::planche, 10}, {ObjetId::structure, 7}, {ObjetId::pve, 1}, {ObjetId::ration, 10}}, {ConstructionId::reseau}));
    constructions.push_back(new Construction(ConstructionId::arroseurs, "Arroseurs auto", 150, 85, false,
                                             {{ObjetId::poutre, 7}, {ObjetId::structure, 15}, {ObjetId::ration, 20}, {ObjetId::tube, 1}}, {ConstructionId::reseau}));
    constructions.push_back(new Construction(ConstructionId::aqua_tourelles, "Aqua tourelles", 70, 60, false,
                                             {{ObjetId::structure, 10}, {ObjetId::ration, 40}, {ObjetId::tube, 7}}, {ConstructionId::pompe}, true));
    constructions.push_back(new Pompe(ConstructionId::percee, "Percée", 2, 12, true,
                                             {{ObjetId::planche, 2}, {ObjetId::ferraille, 2}, {ObjetId::produits, 1}}, {ConstructionId::pompe}));
    constructions.push_back(new Construction(ConstructionId::pluvio_canons, "Pluvio-canons", 80, 40, false,
                                             {{ObjetId::planche, 5}, {ObjetId::ferraille, 5}, {ObjetId::structure, 5}, {ObjetId::ration, 15}}, {ConstructionId::pompe}));
    constructions.push_back(new Construction(ConstructionId::caniveaux, "Caniveaux", 60, 50, false,
                                             {{ObjetId::planche, 10}, {ObjetId::ration, 15}}, {ConstructionId::pompe}));
    constructions.push_back(new Construction(ConstructionId::rid_eau, "Rid'eau", 35, 20, false,
                                             {{ObjetId::ration, 10}}, {ConstructionId::pompe}));
    constructions.push_back(new Pompe(ConstructionId::roquette, "Roquette foreuse", 60, 90, false,
                                             {{ObjetId::poutre, 5}, {ObjetId::structure, 5}, {ObjetId::tube, 1}, {ObjetId::explosifs, 1}, {ObjetId::detonateur, 1}}, {ConstructionId::pompe}));
    constructions.push_back(new Pompe(ConstructionId::detecteur, "Détecteur à eau", 100, 130, false,
                                             {{ObjetId::poutre, 5}, {ObjetId::structure, 10}, {ObjetId::composant, 5}}, {ConstructionId::pompe}));

    // Branche muraille
    constructions.push_back(new Construction(ConstructionId::muraille, "Muraille", 30, 30, false,
                                             {{ObjetId::planche, 15}, {ObjetId::ferraille, 5}}));
    constructions.push_back(new Construction(ConstructionId::grand_fosse, "Grand fossé", 10, 80, false,
                                             listeMateriaux(), {ConstructionId::muraille}, true));
    constructions.push_back(new Construction(ConstructionId::douves, "Douves", 65, 50, false,
                                             {{ObjetId::ration, 20}}, {ConstructionId::grand_fosse}));
    constructions.push_back(new Construction(ConstructionId::muraille_rasoir, "Muraille rasoir", 50, 40, false,
                                             {{ObjetId::ferraille, 15}, {ObjetId::pve, 2}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::fosse_pieux, "Fosse à pieux", 40, 40, false,
                                             {{ObjetId::planche, 8}, {ObjetId::poutre, 4}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::barbeles, "Barbelés", 10, 20, false,
                                             {{ObjetId::ferraille, 2}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::appats, "Appâts", 80, 10, true,
                                             {{ObjetId::os_charnu, 3}}, {ConstructionId::barbeles}));
    constructions.push_back(new Construction(ConstructionId::remparts_avances, "Remparts avancés", 50, 40, false,
                                             {{ObjetId::poutre, 9}, {ObjetId::structure, 6}, {ObjetId::pve, 3}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::poutres_renfort, "Poutres renfort", 25, 40, false,
                                             {{ObjetId::poutre, 1}, {ObjetId::structure, 3}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::seconde_couche, "Seconde couche", 75, 65, false,
                                             {{ObjetId::planche, 35}, {ObjetId::structure, 5}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::oubliettes, "Oubliettes", 35, 50, false,
                                             {{ObjetId::planche, 10}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::barrieres, "Barrières", 30, 50, false,
                                             {{ObjetId::poutre, 5}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::palissade, "Palissade", 45, 50, false,
                                             {{ObjetId::planche, 20}, {ObjetId::poutre, 5}, {ObjetId::pve, 2}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::pulverisateur, "Pulvérisateur", 0, 50, false,
                                             {{ObjetId::ferraille, 10}, {ObjetId::structure, 2}, {ObjetId::pve, 2}, {ObjetId::tube, 1}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::projection_acide, "Projection acide", 40, 30, true,
                                             {{ObjetId::ration, 2}, {ObjetId::produits, 3}}, {ConstructionId::pulverisateur}));
    constructions.push_back(new Construction(ConstructionId::neurotoxine, "Neurotoxine", 150, 40, true,
                                             {{ObjetId::ration, 2}, {ObjetId::produits, 5}, {ObjetId::steroides, 1}}, {ConstructionId::pulverisateur}));
    constructions.push_back(new Construction(ConstructionId::cloison_bois, "Cloison en bois", 25, 30, false,
                                             {{ObjetId::planche, 10}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::cloison_metallique, "Cloison métallique", 25, 30, false,
                                             {{ObjetId::ferraille, 10}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::cloison_epaisse, "Cloison épaisse", 40, 40, false,
                                             {{ObjetId::planche, 10}, {ObjetId::ferraille, 10}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::contre_plaque, "Contre-plaqué", 25, 30, false,
                                             {{ObjetId::planche, 5}, {ObjetId::ferraille, 5}}, {ConstructionId::muraille}));
    constructions.push_back(new Construction(ConstructionId::bastion, "Oubliettes", 45, 30, false,
                                             {{ObjetId::planche, 15}, {ObjetId::ferraille, 15}}, {ConstructionId::muraille}));


    // Instanciation fondations
    constructions.push_back(new Construction(ConstructionId::fondations, "Fondations", 0, 30, false,
                                             {{ObjetId::planche, 10}, {ObjetId::ferraille, 8}}));

    // Branche portail
    constructions.push_back(new Construction(ConstructionId::portail, "Portail", 0, 16, false,
                                             {{ObjetId::ferraille, 2}}));
    constructions.push_back(new Construction(ConstructionId::piston, "Piston calibré", 30, 34, false,
                                             {{ObjetId::planche, 10}, {ObjetId::structure, 3}, {ObjetId::pve, 2}, {ObjetId::tube, 1}}, {ConstructionId::portail}));
    constructions.push_back(new Construction(ConstructionId::blindage, "Blindage d'entrée", 20, 35, false,
                                             {{ObjetId::planche, 3}}, {ConstructionId::portail}));
}

Chantiers::~Chantiers() {
    for (Construction *cons : constructions) {
        delete cons;
    }
    constructions.clear();
    atelier = nullptr;
    tourGuet = nullptr;
    recommande = nullptr;
}

// Méthodes privées
    // Gestion chantier recommandé
Construction* Chantiers::choixRecommande() {
    if (!Ville::getVille()->getCitoyens().empty()) {
        if ((Puits::getInstance()->getQuantite() + Banque::getInstance()->getEau()) / Ville::getVille()->getCitoyens().size() <= 2) {
            Construction *cons = choixPompe();

            if (cons != nullptr) {
                OutreMonde::sortie << "Nouveau chantier recommandé : " << cons->getNom() << std::endl;
                return cons;
            }
        }
        this->miseAJourDefenses();
        if (Ville::getVille()->getDefenses() < tourGuet->getPrevisionMax()) {
            Construction *cons = choixDefenses();

            if (cons != nullptr) {
                OutreMonde::sortie << "Nouveau chantier recommandé : " << cons->getNom() << std::endl;
                return cons;
            }
        }

        Construction *cons = choixTactique();

        if (cons != nullptr) {
            OutreMonde::sortie << "Nouveau chantier recommandé : " << cons->getNom() << std::endl;
            return cons;
        }
    }
    return nullptr;
}
Construction* Chantiers::choixPompe() {
    std::vector<ConstructionId> constructionsPompe =
            {ConstructionId::pompe, ConstructionId::foreuse, ConstructionId::eden, ConstructionId::reseau,
             ConstructionId::percee, ConstructionId::roquette, ConstructionId::detecteur};
    bool temp = true;
    float ratio = 0;
    Pompe *aFaire = nullptr;

    for (ConstructionId id : constructionsPompe) {
        Pompe *cons = (Pompe *) getConstructionParId(id);

        if (cons->isConstruit()) {
            continue;
        }
        else if (cons->isConstructible() && cons->getCoutRation() == 0) {
            return cons;
        }
        else if (!cons->isTemporaire()) {
            temp = false;

            if (cons->getEauSuppl()/cons->getCoutTotal() > ratio) {
                ratio = cons->getEauSuppl()/cons->getCoutTotal();
                aFaire = cons;
            }

        }
        else {
            if (temp) {
                if (cons->getEauSuppl()/cons->getCoutTotal() > ratio) {
                    ratio = cons->getEauSuppl()/cons->getCoutTotal();
                    aFaire = cons;
                }
            }
        }
    }

    return aFaire;
}
Construction* Chantiers::choixDefenses() {
    bool temp = true;
    float ratio = 0;
    Construction *aFaire = nullptr;

    for (Construction *cons : constructions) {
        if (cons->isConstruit()) {
            continue;
        }
        else if (cons->isConstructible() && cons->getCoutRation() == 0) {
            return cons;
        }
        else if (!cons->isTemporaire()) {
            temp = false;

            if ((cons->getDefense()/cons->getCoutTotal()) > ratio) {
                ratio = cons->getDefense()/cons->getCoutTotal();
                aFaire = cons;
            }

        }
        else {
            if (temp) {
                if (cons->getDefense()/cons->getCoutTotal() > ratio) {
                    ratio = cons->getDefense()/cons->getCoutTotal();
                    aFaire = cons;
                }
            }
        }
    }

    return aFaire;
}
Construction* Chantiers::choixTactique() {
    std::vector<ConstructionId> constructionsTactiques =
            {ConstructionId::atelier, ConstructionId::tour_guet, ConstructionId::piston, ConstructionId::potager,
             ConstructionId::cimetiere, ConstructionId::pampl_explo, ConstructionId::fertilisation};

    for (ConstructionId id : constructionsTactiques) {
        Construction *cons = getConstructionParId(id);

        if (cons->isConstruit()) {
            continue;
        }
        else {
            return cons;
        }
    }

    return nullptr;
}

    // Gestion évolution

void Chantiers::choixEvolution() {
    this->miseAJourDefenses();
    std::vector<ConstructionId> evolutions;
    bool evolution = false;

    if (!Ville::getVille()->getCitoyens().empty()) {
        if ((Puits::getInstance()->getQuantite() + Banque::getInstance()->getEau()) /
            Ville::getVille()->getCitoyens().size() <= 1) {
            evolutions = {ConstructionId::pompe, ConstructionId::atelier, ConstructionId::grand_fosse,
                          ConstructionId::supports_def, ConstructionId::aqua_tourelles};
        }
        else if (Ville::getVille()->getDefenses() < tourGuet->getPrevisionMax()) {
            evolutions = {ConstructionId::grand_fosse, ConstructionId::supports_def, ConstructionId::aqua_tourelles,
                          ConstructionId::atelier, ConstructionId::pompe};
        }
        else {
            evolutions = {ConstructionId::atelier, ConstructionId::grand_fosse, ConstructionId::supports_def,
                          ConstructionId::pompe, ConstructionId::aqua_tourelles};
        }

        for (ConstructionId id : evolutions) {
            for (Construction *cons : getChantiersEvolutifsConstruits()) {
                if (cons->getId() == id && !cons->isNiveauMax()) {
                    cons->evoluer();
                    evolution = true;
                    break;
                }
            }

            if (evolution) {
                break;
            }
        }
    }
}

    // Gestion chantiers particuliers
    // Chantiers temporaires
void Chantiers::detruireChantiersTemporaires() {
    for (Construction *cons : constructions) {
        if (cons->isConstruit() && cons->isTemporaire()) {
            cons->detruire();
        }
    }
}

    // Cimetière cadenassé
void Chantiers::defensesCimetiere() {
    Construction *cimetiere = this->getConstructionParId(ConstructionId::cimetiere);

    if (cimetiere != nullptr && cimetiere->isConstruit()) {
        Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() - cimetiere->getDefense());

        cimetiere->setDefense(10 * (40 - (int) Ville::getVille()->getCitoyens().size()));

        Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() + cimetiere->getDefense());
    }
}

    // Piston calibré
void Chantiers::fermeturePortes() {
    if (getConstructionParId(ConstructionId::portail)->isConstruit()) {
        Portes::getInstance()->setOuvrables(false);
    }

    if (getConstructionParId(ConstructionId::piston)->isConstruit()) {
        Portes::getInstance()->setOuvertes(false);
        OutreMonde::sortie << "Portes fermées automatiquement." << std::endl;
    }
}

    // Productions
void Chantiers::productions() {
        potager();
        pamplemoussesExplosifs();
    }
void Chantiers::potager() {
        if (getConstructionParId(ConstructionId::potager)->isConstruit()) {
            int legumes = 0;
            int melons = 0;

            if (!getConstructionParId(ConstructionId::fertilisation)->isConstruit()) {
                legumes = rand() % 4 + 4;
                melons = rand() % 3;
            }
            else {
                legumes = rand() % 3 + 6;
                melons = rand() % 3 + 3;
            }

            OutreMonde::sortie << "Ajout de " << legumes << " Légumes suspects en banque." << std::endl;
            OutreMonde::sortie << "Ajout de " << melons << " Melons d'instestin en banque." << std::endl;
            for (int i = 0; i < legumes; ++i) {
                Banque::getInstance()->ajouterObjet(ObjetId::legume);
            }
            for (int i = 0; i < melons; ++i) {
                Banque::getInstance()->ajouterObjet(ObjetId::melon);
            }
        }
    }
void Chantiers::pamplemoussesExplosifs() {
    if (getConstructionParId(ConstructionId::pampl_explo)->isConstruit()) {
        int pamplemousses = 0;

        if (!getConstructionParId(ConstructionId::fertilisation)->isConstruit()) {
            pamplemousses = rand() % 5 + 3;
        }
        else {
            pamplemousses = rand() % 4 + 5;
        }

        OutreMonde::sortie << "Ajout de " << pamplemousses << " Pamplemousses explosifs en banque." << std::endl;
        for (int i = 0; i < pamplemousses; ++i) {
            Banque::getInstance()->ajouterObjet(ObjetId::pampl_explo);
        }
    }
}

    // Supports défensifs
void Chantiers::supportsDefensifs() {
        Construction *supports = getConstructionParId(ConstructionId::supports_def);
    if (supports->isConstruit()) {
        Ville::getVille()->setDefenses(Ville::getVille()->getDefenses() * (1.10 + 0.02 * supports->getNiveau()));
    }
}

    // Mise à jour des défenses

void Chantiers::miseAJourDefenses() {
    this->defensesCimetiere();
    this->supportsDefensifs(); // Todo: ajouter aqua-tourelles
}
void Chantiers::constructionsFinies() {
    for (Construction *cons : constructions) {
        cons->enConstruction(0);
    }
}
// Fin méthodes privées

void Chantiers::changementJournee(bool avantAttaque) {
    if (avantAttaque) {
        this->fermeturePortes();
        this->miseAJourDefenses();
        this->choixEvolution();
    }
    else {
        this->constructionsFinies();
        this->detruireChantiersTemporaires();
        this->productions();
        recommande = this->choixRecommande();
        tourGuet->reinitialiser();
    }
}

std::vector<Construction *> Chantiers::getConstructibles() {
    std::vector<Construction *> constructibles;

    for (Construction *cons : constructions) {
        if (cons->estConstructible()) {
            constructibles.push_back(cons);
        }
    }

    return constructibles;
}

Construction *Chantiers::getConstructionParId(ConstructionId id) {
    std::vector<Construction *>::iterator i;
    for (i = this->constructions.begin(); i != constructions.end() && (*i)->getId() != id; ++i);

    return *i;
}

std::vector<Construction *> Chantiers::getChantiersEvolutifsConstruits() {
    std::vector<Construction *> evolutifs;

    for (Construction *cons : constructions) {
        if (cons->isConstruit() && cons->isEvolutif()) {
            evolutifs.push_back(cons);
        }
    }

    return evolutifs;
}

std::vector<ConstructionId> Chantiers::chantiersConstruits() {
    std::vector<ConstructionId> construits;
    for (Construction *cons : constructions) {

        if (cons->isConstruit()) {
            construits.push_back(cons->getId());
        }
    }

    return construits;
}