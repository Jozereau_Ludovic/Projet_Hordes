//
// Created by jozereau on 17/10/16.
//

#include "../../../ressources/outremonde/ville/Ville.hpp"

Ville *Ville::ville = nullptr;

Ville::Ville(std::vector<Citoyen *> citoyens) :
        portes(Portes::getInstance()), banque(Banque::getInstance()), puits(Puits::getInstance()), chantiers(
        Chantiers::getInstance()),
        citoyens(citoyens), expeditions(std::vector<Expedition *>()), defenses(0) {
}

Ville::~Ville() {
    Portes::destroy();
    Banque::destroy();
    Puits::destroy();
    Chantiers::destroy();

    for (Expedition *expe : expeditions) {
        delete expe;
    }
    expeditions.clear();

    for (Citoyen *citoyen : citoyens)
        delete citoyen;
}

Ville* Ville::getVille(std::vector<Citoyen *> citoyens) {
    if (ville == nullptr) {
        ville = new Ville(citoyens);
    }

    return ville;
}

void Ville::killVille() {
    if (ville != nullptr) {
        delete ville;
        ville = nullptr;
    }
}

void Ville::brasserOrdre() {
    std::vector<Citoyen *> ordre = citoyens;
    citoyens.clear();

    while (!ordre.empty()) {
        int indice = rand()%ordre.size();
        citoyens.push_back(ordre[indice]);
        ordre.erase(ordre.begin() + indice);
    }
}

void Ville::changementJournee() {
    for (std::vector<Citoyen *>::const_iterator i = citoyens.begin(); i != citoyens.end(); ) {
        if (!(*i)->changementJournee()) {
            ++i;
        }
    }
}

bool Ville::actionsTerminees() {
    for (Citoyen *citoyen : citoyens) {
        if (citoyen->getComportement() != nullptr && citoyen->getComportement()->getProchaineAction() != ActionId::fin) {
            return false;
        }
    }

    return true;
}

bool Ville::citoyenVivant() {
    return !citoyens.empty();
}

void Ville::citoyenMort(int id) {
    for (std::vector<Citoyen *>::iterator i = citoyens.begin(); i != citoyens.end(); ++i) {
        if ((*i)->getId() == id) {
            Citoyen *citoyen = *i;
            *i = nullptr;
            citoyens.erase(i);
            delete citoyen;
            break;
        }
    }
}

void Ville::ajouterExpedition(Expedition *expe) {
    expeditions.push_back(expe);
}

void Ville::supprimerExpedition(int id) {
    for (std::vector<Expedition *>::iterator i = expeditions.begin(); i != expeditions.end(); ++i) {
        if ((*i)->getId() == id) {
            Expedition *expe = *i;
            *i = nullptr;
            expeditions.erase(i);
            delete expe;
            break;
        }
    }
}