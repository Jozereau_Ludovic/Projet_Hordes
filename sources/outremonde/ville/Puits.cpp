//
// Created by jozereau on 18/10/16.
//

#include "../../../ressources/objets/Consommable.hpp"
#include "../../../ressources/outremonde/ville/Puits.hpp"
#include "../../../ressources/divers/Gauss.h"
#include "../../../ressources/objets/ListeObjets.hpp"

Puits::Puits() : quantite((int) Gauss::getInstance()->geneGauss(135, 26)), pompe(false) { }

Puits::~Puits() { }

Consommable *Puits::prendreRation() {
    quantite--;
    return (Consommable *) ListeObjets::getInstance()->getObjetsParId(ObjetId::ration);
}
