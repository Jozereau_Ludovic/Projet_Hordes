//
// Created by jozereau on 17/10/16.
//

#include "../../../ressources/outremonde/ville/Banque.hpp"
#include "../../../ressources/objets/ListeObjets.hpp"

Banque::Banque() : objets(listeBanque()) {}

Banque::~Banque() {
    objets.clear();
}

void Banque::ajouterObjet(ObjetId objet) {
    objets[objet]++;
}

void Banque::enleverObjet(ObjetId objet) {
    if (objets[objet] > 0) {
        objets[objet]--;
    }
}

int Banque::getEau() {
    int nbEau = 0;

    for (listeBanque::iterator i = objets.begin(); i != objets.end(); ++i) {
        Objet *obj = ListeObjets::getInstance()->getObjetsParId((*i).first);

        if (obj->getTypeObjet() == TypeObjet::consommable) {
            Consommable *consommable = (Consommable *) obj;

            if (consommable->isEau()) {
                nbEau += (*i).second;
            }
        }

        delete obj;
    }

    return nbEau;
}