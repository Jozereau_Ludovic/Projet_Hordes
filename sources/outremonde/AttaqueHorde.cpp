//
// Created by jozereau on 05/03/17.
//

#include <stdlib.h>
#include <cmath>
#include "../../ressources/outremonde/AttaqueHorde.hpp"

AttaqueHorde::AttaqueHorde() : attaqueMin(20), attaqueMax(30) { }

int AttaqueHorde::execute(int jour) {
    int zombies = rand()%(attaqueMax - attaqueMin + 1) + attaqueMin;

    attaqueMin = 17.8 * std::pow(jour, 2) - 142.8 * jour + 331.4;
    attaqueMax = 16.6 * std::pow(jour, 2) - 28.8 * jour + 324.5;

    return zombies;
}