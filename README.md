<h1>Projet ISIMA - Projet Hordes</h1>

<p><h2>Description du travail</h2>
Simulation du monde et des règles du jeu Hordes.fr. Simulation d'agents aux comportements
semblables à ceux des joueurs classiques dans le but de voir la durée de la survie en moyenne.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>CLion
    <li>Qt
    <li>C++
</ul></p>

<p><h2>Méthode de travail</h2>
<ul><li>Méthodologie Agile.
</ul></p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Leader Programmeur
    <li>Concepteur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Nicolas Auguste
    <li>Ludovic Jozereau
</ul></p>

<p><h3>Pas d'exécutable disponible actuellement.</h3></p>