//
// Created by jozereau on 20/02/17.
//

#ifndef PROJETHORDES_CONTENEUR_HPP
#define PROJETHORDES_CONTENEUR_HPP


#include <vector>
#include "Objet.hpp"

class Conteneur : public Objet {
private:
    std::vector<std::pair<ObjetId, float>> drops; // Objet et pourcentage d'obtention
    std::vector<ObjetId> outils;

    Objet *drop;

public:
    Conteneur(ObjetId id, std::string nom, Rarete rarete, std::vector<std::pair<ObjetId, float>> drops, std::vector<ObjetId> outils);
    Conteneur(const Conteneur &conteneur);
    ~Conteneur();

    // Getteurs et setteurs
    const std::vector<std::pair<ObjetId, float>> &getDrops() const {
        return drops;
    }
    void setDrops(const std::vector<std::pair<ObjetId, float>> &drops) {
        Conteneur::drops = drops;
    }

    const std::vector<ObjetId> &getOutils() const {
        return outils;
    }
    void setOutils(const std::vector<ObjetId> &outils) {
        Conteneur::outils = outils;
    }

    Objet *getDrop() const {
        return drop;
    }
    void setDrop(Objet *drop) {
        Conteneur::drop = drop;
    }
    // Fin getteurs et setteurs

    TypeObjet utiliser(listeObjet inventaire);
    TypeObjet getTypeObjet() {
        return TypeObjet::conteneur;
    }
    bool outilPresent(listeObjet inventaire);
};


#endif //PROJETHORDES_CONTENEUR_HPP
