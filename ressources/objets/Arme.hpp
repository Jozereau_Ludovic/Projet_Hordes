//
// Created by jozereau on 05/03/17.
//

#ifndef PROJETHORDES_ARME_HPP
#define PROJETHORDES_ARME_HPP


#include "Objet.hpp"

class Arme : public Objet {
private:
    float probaTouche;
    int minTue;
    int maxTue;

    bool recharge;
    float probaDecharge;
    int chargesMax;
    int charges;
    ObjetId munitions;

    bool casse;

    int attaquerNonRecharge();
    int attaquerRecharge();
public:
    Arme(ObjetId id, std::string nom, Rarete rarete, int destructibilite, float probaTouche, int minTue, int maxTue,
         bool casse = false);
    Arme(ObjetId id, std::string nom, Rarete rarete, int destructibilite, int minTue, int maxTue, float probaDecharge,
         int chargesMax, int charges, ObjetId munitions);
    Arme(const Arme &arme);

    // Getteurs et setteurs
    float getProbaTouche() const {
        return probaTouche;
    }
    void setProbaTouche(float probaTouche) {
        Arme::probaTouche = probaTouche;
    }

    int getMinTue() const {
        return minTue;
    }
    void setMinTue(int minTue) {
        Arme::minTue = minTue;
    }

    int getMaxTue() const {
        return maxTue;
    }
    void setMaxTue(int maxTue) {
        Arme::maxTue = maxTue;
    }

    bool isRecharge() const {
        return recharge;
    }
    void setRecharge(bool recharge) {
        Arme::recharge = recharge;
    }

    float getProbaDecharge() const {
        return probaDecharge;
    }
    void setProbaDecharge(float probaDecharge) {
        Arme::probaDecharge = probaDecharge;
    }

    int getChargesMax() const {
        return chargesMax;
    }
    void setChargesMax(int chargesMax) {
        Arme::chargesMax = chargesMax;
    }

    int getCharges() const {
        return charges;
    }
    void setCharges(int charges) {
        Arme::charges = charges;
    }

    const ObjetId &getMunitions() const {
        return munitions;
    }
    void setMunitions(const ObjetId &munitions) {
        Arme::munitions = munitions;
    }

    bool isCasse() const {
        return casse;
    }
    void setCasse(bool casse) {
        Arme::casse = casse;
    }
    // Fin getteurs et setteurs

    int nombreTues();
    bool isVide();

    void recharger();

    TypeObjet utiliser(listeObjet inventaire);
    TypeObjet getTypeObjet() {
        return TypeObjet::arme;
    }

};


#endif //PROJETHORDES_ARME_HPP
