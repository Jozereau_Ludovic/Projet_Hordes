//
// Created by jozereau on 08/12/16.
//

#ifndef PROJETHORDES_CONSOMMABLE_HPP
#define PROJETHORDES_CONSOMMABLE_HPP

#include "Objet.hpp"

typedef std::vector<std::pair<EffetId, EffetId>> listeEffets;

class Consommable : public Objet {
private:
    int pa_bonus; //Nombre de pa bonus
    bool pa_rendu;

    // Modification des états
    bool eau; // Modifie les états liés à l'eau
    bool nourriture; // Modifie les états liés à la nourriture
    bool alcool; // Modifie les états liés à l'alcool
    bool drogue; // Modifie les états liés à la drogue
    bool terreur;
    bool infection;
    bool soin;

    // Effet
    bool aleatoire; // Indique si l'effet est aléatoire
    listeEffets effets;
    bool dependance;


public:
    Consommable(ObjetId id, std::string nom, Rarete rarete, listeEffets effets); // Constructeur aléatoire
    Consommable(ObjetId id, std::string nom, Rarete rarete, int pa_bonus, bool eau, bool nourriture = false, bool alcool = false,
                bool drogue = false, bool terreur = false, bool infection = false, bool soin = false);
    Consommable(const Consommable &consommable);
    ~Consommable();

    // Getteurs et setteurs
    int getPa_bonus() const {
        return pa_bonus;
    }
    void setPa_bonus(int pa_bonus) {
        Consommable::pa_bonus = pa_bonus;
    }

    bool isPa_rendu() const {
        return pa_rendu;
    }
    void setPa_rendu(bool pa_rendu) {
        Consommable::pa_rendu = pa_rendu;
    }

    bool isEau() const {
        return eau;
    }
    void setEau(bool eau) {
        Consommable::eau = eau;
    }

    bool isNourriture() const {
        return nourriture;
    }
    void setNourriture(bool nourriture) {
        Consommable::nourriture = nourriture;
    }

    bool isAlcool() const {
        return alcool;
    }
    void setAlcool(bool alcool) {
        Consommable::alcool = alcool;
    }

    bool isDrogue() const {
        return drogue;
    }
    void setDrogue(bool drogue) {
        Consommable::drogue = drogue;
    }

    bool isTerreur() const {
        return terreur;
    }
    void setTerreur(bool terreur) {
        Consommable::terreur = terreur;
    }

    bool isInfection() const {
        return infection;
    }
    void setInfection(bool infection) {
        Consommable::infection = infection;
    }

    bool isSoin() const {
        return soin;
    }
    void setSoin(bool soin) {
        Consommable::soin = soin;
    }

    bool isAleatoire() const {
        return aleatoire;
    }
    void setAleatoire(bool aleatoire) {
        Consommable::aleatoire = aleatoire;
    }

    const listeEffets &getEffets() const {
        return effets;
    }
    void setEffets(const listeEffets &effets) {
        Consommable::effets = effets;
    }

    bool isDependance() const {
        return dependance;
    }
    void setDependance(bool dependance) {
        Consommable::dependance = dependance;
    }
    // Fin getteurs et setteurs

    TypeObjet utiliser(listeObjet inventaire);
    TypeObjet getTypeObjet() {
        return TypeObjet::consommable;
    }
    void resetEffets();
    void effetsAleatoire(EffetId effet);
};

#endif //PROJETHORDES_CONSOMMABLE_HPP
