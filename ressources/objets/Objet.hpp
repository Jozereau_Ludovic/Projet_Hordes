//
// Created by jozereau on 18/10/16.
//

#ifndef PROJETHORDES_OBJET_HPP
#define PROJETHORDES_OBJET_HPP

#include <string>
#include <iostream>
#include <memory>
#include "../divers/donnees.hpp"
#include "../divers/outils.hpp"

class Objet {
protected:
    ObjetId id; // Identifiant de l'objet
    std::string nom;
    Rarete rarete;
    int destructibilite;

public:
    Objet();
    Objet(ObjetId id, std::string nom, Rarete rarete, int destructibilite = 0);
    Objet(const Objet &objet);
    virtual ~Objet();

    // Getteurs et setteurs
    const ObjetId &getId() const { return id; }
    void setId(const ObjetId &id) { Objet::id = id; }

    const std::string &getNom() const { return nom; }
    void setNom(const std::string &nom) { Objet::nom = nom; }

    const Rarete &getRarete() const { return rarete; }
    void setRarete(const Rarete &rarete) { Objet::rarete = rarete; }

    int getDestructibilite() const { return destructibilite; }
    void setDestructibilite(int destructibilite) { Objet::destructibilite = destructibilite; }
    // Fin getteurs et setteurs

    bool detruireObjet();
    virtual TypeObjet utiliser(listeObjet inventaire);
    virtual TypeObjet getTypeObjet() {
        return TypeObjet::objet;
    }
};

bool plusRare(Objet *const &a, Objet *const &b);

#endif //PROJETHORDES_OBJET_HPP
