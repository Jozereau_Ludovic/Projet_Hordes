//
// Created by jozereau on 06/02/17.
//

#ifndef PROJETHORDES_LISTEOBJETS_HPP
#define PROJETHORDES_LISTEOBJETS_HPP


#include <vector>
#include "Objet.hpp"
#include "Consommable.hpp"
#include "Conteneur.hpp"
#include "Arme.hpp"
#include "../divers/Singleton.hpp"

class ListeObjets : public Singleton<ListeObjets> {
    friend class Singleton<ListeObjets>;

private:
     ListeObjets();
    ~ListeObjets();

    std::vector<Objet *> tousObjets;
    std::vector<Objet *> objets;
    std::vector<Consommable *> consommables;
    std::vector<Conteneur *> conteneurs;
    std::vector<Arme *> armes;

    template <typename T>
    T getObjetParId(ObjetId id, std::vector<T> liste);

public:
    // Getteurs et setteurs
    const std::vector<Objet *> &getTousObjets() const {
        return tousObjets;
    }
    void setTousObjets(const std::vector<Objet *> &tousObjets) {
        ListeObjets::tousObjets = tousObjets;
    }

    const std::vector<Consommable *> &getConsommables() const {
        return consommables;
    }
    void setConsommables(const std::vector<Consommable *> &consommables) {
        ListeObjets::consommables = consommables;
    }

    const std::vector<Conteneur *> &getConteneurs() const {
        return conteneurs;
    }
    void setConteneurs(const std::vector<Conteneur *> &conteneurs) {
        ListeObjets::conteneurs = conteneurs;
    }
    // Fin getteurs et setteurs


    Objet *getObjetsParId(ObjetId id);

    const std::vector<Objet*> getObjetsParRarete(Rarete r);
};


#endif //PROJETHORDES_LISTEOBJETS_HPP
