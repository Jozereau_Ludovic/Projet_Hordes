// Created by jozereau on 09/02/17.
//

#ifndef PROJETHORDES_COMPOUVRIER_HPP
#define PROJETHORDES_COMPOUVRIER_HPP


#include "Comportement.hpp"

class CompOuvrier : public Comportement {
private:
    void rechargerPa();
    void fouiller();
    void ranger();
    void preparation();
    void ressources();
    void construire();
    void atelier();
    void tourGuet();
    void creerExpedition();

public:
    CompOuvrier(Citoyen *citoyen);

    void reinitialiser();
};


#endif //PROJETHORDES_COMPOUVRIER_HPP
