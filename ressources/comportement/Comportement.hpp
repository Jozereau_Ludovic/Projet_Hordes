//
// Created by jozereau on 09/02/17.
//

#ifndef PROJETHORDES_COMPORTEMENT_HPP
#define PROJETHORDES_COMPORTEMENT_HPP

#include "../outremonde/ville/chantiers/Construction.hpp"
#include "../Expedition.hpp"

class Citoyen;

class Comportement {
protected:
    Citoyen *citoyen;
    ActionId prochaineAction;
    Expedition *expedition;

    int posTrajet;

    int fouilleAuto;
    bool fouille;

    virtual void entrer();
    virtual void sortir();
    virtual void rechargerPa() = 0;
    virtual void fouiller() = 0;
    virtual void seDeplacer();
    virtual void creerExpedition() = 0;
    virtual void ranger();
    virtual void preparation() = 0;
    virtual void ressources() = 0;
    virtual void construire() = 0;
    virtual void atelier() = 0;
    virtual void ramasser();
    virtual void tourGuet();

public:
    Comportement(Citoyen *citoyen);
    virtual ~Comportement();

    // Getteurs et setteurs
    Citoyen *getCitoyen() const {
        return citoyen;
    }
    void setCitoyen(Citoyen *citoyen) {
        Comportement::citoyen = citoyen;
    }

    const ActionId &getProchaineAction() const {
        return prochaineAction;
    }
    void setProchaineAction(const ActionId &prochaineAction) {
        Comportement::prochaineAction = prochaineAction;
    }

    Expedition *getExpedition() const {
        return expedition;
    }
    void setExpedition(Expedition *expedition) {
        Comportement::expedition = expedition;
    }
    // Fin getteurs et setteurs

    virtual void execute();
    virtual void reinitialiser() = 0;
    virtual Construction *choisirConstruction();
};


#endif //PROJETHORDES_COMPORTEMENT_HPP
