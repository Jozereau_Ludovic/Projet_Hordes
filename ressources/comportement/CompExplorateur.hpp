//
// Created by jozereau on 23/02/17.
//

#ifndef PROJETHORDES_COMPEXPLORATEUR_HPP
#define PROJETHORDES_COMPEXPLORATEUR_HPP

#include "Comportement.hpp"

class CompExplorateur : public Comportement {
private:
    void rechargerPa();
    void fouiller();
    void preparation();
    void ressources();
    void construire();
    void atelier();
    void tourGuet();
    void creerExpedition();

public:
    CompExplorateur(Citoyen *citoyen);
    void reinitialiser();
};

#endif //PROJETHORDES_COMPEXPLORATEUR_HPP
