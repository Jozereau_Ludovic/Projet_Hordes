//
// Created by jozereau on 23/02/17.
//

#ifndef PROJETHORDES_EXPEDITION_HPP
#define PROJETHORDES_EXPEDITION_HPP


#include <vector>
#include "outremonde/Case.hpp"

class Citoyen;

class Expedition {
private:
    static int compteur;
    int id;
    int paTotaux;
    int membresMax;

    std::vector<Citoyen *> membres;
    std::vector<Case *> trajet;

public:
    Expedition(int pa, int membresMax, Citoyen *citoyen);
    ~Expedition();

    // Getteurs et setteurs
    static int getCompteur() {
        return compteur;
    }
    static void setCompteur(int compteur) {
        Expedition::compteur = compteur;
    }

    int getId() const {
        return id;
    }
    void setId(int id) {
        Expedition::id = id;
    }

    int getPaTotaux() const {
        return paTotaux;
    }
    void setPaTotaux(int paTotaux) {
        Expedition::paTotaux = paTotaux;
    }

    int getMembresMax() const {
        return membresMax;
    }
    void setMembresMax(int membresMax) {
        Expedition::membresMax = membresMax;
    }

    const std::vector<Citoyen *> &getMembres() const {
        return membres;
    }
    void setMembres(const std::vector<Citoyen *> &membres) {
        Expedition::membres = membres;
    }

    const std::vector<Case *> &getTrajet() const {
        return trajet;
    }
    void setTrajet(const std::vector<Case *> &trajet) {
        Expedition::trajet = trajet;
    }
    // Fin getteurs et setteurs

    Case *getCaseSuivante(int pos);
    bool tousRentres();
    void ajouterMembre(Citoyen *citoyen);

    std::vector<Case *> eloignementVille(Case *position);
    std::vector<Case *> rapprochementVille(Case *position);
    std::vector<Case *> filtreNouvellesCases(std::vector<Case *> cases);

    bool complete();

};


#endif //PROJETHORDES_EXPEDITION_HPP
