//
// Created by dyag on 2/7/17.
//

#ifndef PROJETHORDES_TEST_H
#define PROJETHORDES_TEST_H

#include "outremonde/OutreMonde.hpp"

TEST (Initialisation, Init) {
    OutreMonde *pTest = new OutreMonde();

    EXPECT_NE(pTest, nullptr);
    EXPECT_NE(Ville::getVille(), nullptr);
    EXPECT_GE(Ville::getVille()->getCitoyens().size(),1);
    EXPECT_NE(Ville::getVille()->getBanque(), nullptr);
    EXPECT_NE(Ville::getVille()->getPuits(), nullptr);
    EXPECT_NE(Ville::getVille()->getChantiers(), nullptr);
    EXPECT_NE(Ville::getVille()->getPortes(), nullptr);

    delete(pTest);
}

TEST (Banque, AjoutRessourcesParDieu) {
    Banque::getInstance()->ajouterObjet(ObjetId::tube);
    EXPECT_EQ(Banque::getInstance()->getNbObjets(ObjetId::tube),1);
    Banque::destroy();
}

TEST (Puit,SansPompe) {
    OutreMonde world;
    std::vector<Citoyen*> citoyens = Ville::getVille()->getCitoyens();

    EXPECT_EQ(Puits::getPuits()->getQuantite(),150);
    citoyens[0]->prendreRation();
    EXPECT_EQ(citoyens[0]->getInventaire()[0]->getId(),ObjetId::ration);
    EXPECT_EQ(Puits::getPuits()->getQuantite(),149);
    citoyens[0]->prendreRation();
    EXPECT_NE(citoyens[0]->getInventaire().size(),2);
    EXPECT_NE(Puits::getPuits()->getQuantite(),148);
}

TEST (Banque, PoserRessourcesEnBanque) {
    OutreMonde world;

    std::vector<Citoyen*> citoyens = Ville::getVille()->getCitoyens();
    citoyens[0]->prendreRation();
    citoyens[0]->poserBanque(citoyens[0]->getInventaire()[0]);
    EXPECT_EQ(citoyens[0]->getInventaire().size(),0);
    EXPECT_EQ(Banque::getInstance()->getNbObjets(ObjetId::ration),1);
}

TEST (Construction, ConstructionChantier) {
    OutreMonde world;

    EXPECT_EQ(Ville::getVille()->getChantiers()->getPompe()->estConstructible(),false);

    Banque::getInstance()->ajouterObjet(ObjetId::tube);
    for (int i=0;i<8;i++) {
        Banque::getInstance()->ajouterObjet(ObjetId::ferraille);
    }

    EXPECT_EQ(Ville::getVille()->getChantiers()->getPompe()->estConstructible(),true);
    int pa = Ville::getVille()->getChantiers()->getPompe()->enConstruction(100);
    EXPECT_EQ(pa,100-Chantiers::getInstance()->getPompe()->getPaTotaux());
    EXPECT_EQ(Chantiers::getChantiers()->getPompe()->isConstruit(),true);
    EXPECT_EQ(Chantiers::getChantiers()->getPompe()->estConstructible(),false);
    EXPECT_EQ(Banque::getInstance()->getNbObjets(ObjetId::tube),0);
    EXPECT_EQ(Banque::getInstance()->getNbObjets(ObjetId::ferraille),0);
}

TEST (Puit,AvecPompe) {
    OutreMonde world;
    std::vector<Citoyen*> citoyens = Ville::getVille()->getCitoyens();

    Chantiers::getInstance()->getPompe()->setConstruit(true);
    citoyens[0]->prendreRation();
    citoyens[0]->prendreRation();
    EXPECT_EQ(Puits::getPuits()->getQuantite(),148);
    EXPECT_EQ(citoyens[0]->getInventaire().size(),2);
    citoyens[0]->prendreRation();
    EXPECT_EQ(citoyens[0]->getInventaire().size(),2);
    EXPECT_EQ(Puits::getPuits()->getQuantite(),148);
}

#endif //PROJETHORDES_TEST_H
