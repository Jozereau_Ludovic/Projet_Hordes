//
// Created by jozereau on 26/10/16.
//

#ifndef PROJETHORDES_OUTILS_HPP
#define PROJETHORDES_OUTILS_HPP

#include <vector>

class Objet;

typedef std::vector<Objet *> listeObjet;
void transfertObjet(listeObjet depart, listeObjet arrivee, Objet *objet);

#endif //PROJETHORDES_OUTILS_HPP
