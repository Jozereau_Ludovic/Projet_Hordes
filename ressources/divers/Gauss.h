//
// Created by dyag on 2/6/17.
//

#ifndef PROJETHORDES_GAUSS_H
#define PROJETHORDES_GAUSS_H


#include <random>
#include "Singleton.hpp"

class Gauss : public Singleton<Gauss> {
    friend class Singleton<Gauss>;

private:
    std::default_random_engine gene;
    Gauss();
    ~Gauss();

public:
    float geneGauss(float, float);
};


#endif //PROJETHORDES_GAUSS_H
