//
// Created by jozereau on 17/10/16.
//

#ifndef PROJETHORDES_DONNEES_HPP
#define PROJETHORDES_DONNEES_HPP

#include <fstream>
#include <iostream>

enum class Blessure {tete, oeil, bras, main, jambe, pied};
enum class Eau {aucun, soif, deshydrate};
enum class Alcool {aucun, ivre, gueuleDeBois};

enum class Direction {nord, sud, est, ouest};
enum class Rarete {debris, commun, rare, epique, unique, introuvable, ration};

enum class TypeObjet {objet, consommable, conteneur, arme};
enum class EffetId {rien, dependance, pa, pa_bonus, terreur, infection};

enum class ObjetId : int {
    /** Objets **/
    souche, planche, poutre, debris, ferraille, structure, tube, scie, lampe_off, pile, radio_off, produits, allumettes,
    meuble_kit, rocking, table, treteau, porte, mecanisme, pve, rustine, explosifs, detonateur, composant,

    /** Consommables **/
    ration, biscuit, poulet, chewing, jambon_beurre, napolitain, nouilles, chips, petit_beurre, pims, legume,
    steak, melon, vodka, steroides, twinoide, mse, os_charnu,

    /** Conteneurs **/
    doggybag, affaires,

    /** Armes **/
        // Sans recharge
    pampl_explo,
    canif, canif_casse, chaise, chaise_casse, clef_molette, clef_molette_casse, coupe_coupe, coupe_coupe_casse,
    couteau_dents, couteau_dents_casse, couteau_suisse, couteau_suisse_casse, cutter, cutter_casse, four, four_casse,
    baton, baton_casse, chaine, chaine_casse, tournevis, tournevis_casse,

        // A recharges
    lance_pile_1_charge, lance_pile_1_vide,
    pistolet_eau_3, pistolet_eau_2, pistolet_eau_1, pistolet_eau_vide
};

enum class ActionId {
    debut, entrer, sortir, rechargerPa, fouiller, seDeplacer, expedition, ranger,
    preparation, ressources, construire, atelier, ramasser, tourGuet, fin
};

enum class ConstructionId {
    // Branche atelier
    atelier, tourniquet, manufacture, scies, supports_def, cimetiere,

    // Banque tour de guet
    tour_guet,

    // Branche pompe
    pompe, potager, pampl_explo, fertilisation, foreuse, eden, reseau, vaporisateur, arroseurs,
    aqua_tourelles, percee, pluvio_canons, caniveaux, rid_eau, roquette, detecteur,

    // Branche muraille
    muraille, grand_fosse, douves, muraille_rasoir, fosse_pieux, barbeles, appats, remparts_avances,
    poutres_renfort, seconde_couche, oubliettes, barrieres, palissade, pulverisateur, projection_acide, neurotoxine,
    cloison_bois, cloison_metallique, cloison_epaisse, contre_plaque, bastion,

    // Branche fondations
    fondations,

    // Branche portail
    portail, piston, blindage
};

enum class MortId {
    deshydratation, infection, dependance, attaque_ville, attaque_desert
};

#endif //PROJETHORDES_DONNEES_HPP
