//
// Created by dyag on 20/10/16.
//

#ifndef PROJETHORDES_FONCTIONGLOBALE_HPP
#define PROJETHORDES_FONCTIONGLOBALE_HPP

float generateurAleatoire (float min, float max);
int generateurAleatoireInt(int min, int max);
float geneGauss (float sigma, float lampda);

#endif //PROJETHORDES_FONCTIONGLOBALE_HPP
