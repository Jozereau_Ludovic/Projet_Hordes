//
// Created by dyag on 20/10/16.
//

#ifndef PROJETHORDES_OUTREMONDE_HPP
#define PROJETHORDES_OUTREMONDE_HPP

#include <iostream>
#include <vector>

#include "Case.hpp"
#include "ville/Ville.hpp"
#include "AttaqueHorde.hpp"
#include "../divers/Singleton.hpp"


class OutreMonde : public Singleton<OutreMonde> {
    friend class Singleton<OutreMonde>;
protected:
    OutreMonde();
    ~OutreMonde();

    int taille;

    int posVilleX;
    int posVilleY;

    std::vector< std::vector<Case*> > desert;
    Ville* city;

    int temps; // Unite : 1 minute
    bool nuit; // Entre 19h et 7h
    int jour;

    AttaqueHorde *attaqueHorde;
    int nombreZombie;
    int multiplicateur;


public:
    static std::ofstream sortie;

    const int getCoordVilleX();
    const int getCoordVilleY();

    // Getteurs et setteurs
    const std::vector<std::vector<Case *>> &getDesert() const {
        return desert;
    }

    int getTemps() const {
        return temps;
    }
    void setTemps(int temps) {
        OutreMonde::temps = temps;
    }

    bool isNuit() const {
        return nuit;
    }
    void setNuit(bool nuit) {
        OutreMonde::nuit = nuit;
    }

    int getJour() const {
        return jour;
    }

    int getNombreZombie() const {
        return nombreZombie;
    }
    // Fin getteurs et setteurs


    Case *getCase(int x, int y);

    void proliferation();
    void affichageTexte();

    void changementJournee();
    void avancerTemps();
    void gestionAttaqueHorde();
};

#endif //PROJETHORDES_OUTREMONDE_HPP
