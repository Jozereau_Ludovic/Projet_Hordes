// Created by jozereau on 05/03/17.
//

#ifndef PROJETHORDES_ATTAQUEHORDE_HPP
#define PROJETHORDES_ATTAQUEHORDE_HPP


class AttaqueHorde {
private:
    int attaqueMin;
    int attaqueMax;

public:
    AttaqueHorde();

    int execute(int jour);

};


#endif //PROJETHORDES_ATTAQUEHORDE_HPP
