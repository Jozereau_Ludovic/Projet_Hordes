//
// Created by jozereau on 17/10/16.
//

#ifndef PROJETHORDES_BANQUE_HPP
#define PROJETHORDES_BANQUE_HPP


#include <map>
#include "../../divers/donnees.hpp"
#include "../../objets/Objet.hpp"
#include "../../divers/Singleton.hpp"

typedef std::map<ObjetId, int> listeBanque; // Id d'un objet associé à sa quantité

class Banque : public Singleton<Banque> {
    friend class Singleton<Banque>;

private:
    Banque();
    ~Banque();

    listeBanque objets;

public:
    // Getteurs et setteurs
    const listeBanque &getObjets() const {
        return objets;
    }

    listeBanque &getObjets() {
        return objets;
    }

    void setObjets(const listeBanque &objets) {
        Banque::objets = objets;
    }
    // Fin getteurs et setteurs

    int getNbObjets(ObjetId id) {
        return objets[id];
    }
    void setNbObjets(ObjetId id, int nombre) {
        objets[id] = nombre;
    }

    void ajouterObjet(ObjetId objet);
    void enleverObjet(ObjetId objet);

    int getEau();

};

#endif //PROJETHORDES_BANQUE_HPP
