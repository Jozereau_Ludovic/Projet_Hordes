//
// Created by jozereau on 06/03/17.
//

#ifndef PROJETHORDES_TOURGUET_HPP
#define PROJETHORDES_TOURGUET_HPP


#include "Construction.hpp"

class TourGuet : public Construction {
private :
    double precision;

    int incertitude;
    int previsionMin;
    int previsionMax;

    double coefficient;

public :
    TourGuet();

    // Getteurs et setteurs
    double getPrecision() const {
        return precision;
    }
    void setPrecision(double precision) {
        TourGuet::precision = precision;
    }

    int getIncertitude() const {
        return incertitude;
    }
    void setIncertitude(int incertitude) {
        TourGuet::incertitude = incertitude;
    }

    int getPrevisionMin() const {
        return previsionMin;
    }
    void setPrevisionMin(int previsionMin) {
        TourGuet::previsionMin = previsionMin;
    }

    int getPrevisionMax() const {
        return previsionMax;
    }
    void setPrevisionMax(int previsionMax) {
        TourGuet::previsionMax = previsionMax;
    }

    double getCoefficient() const {
        return coefficient;
    }
    void setCoefficient(double coefficient) {
        TourGuet::coefficient = coefficient;
    }
    // Fin getteurs et setteurs

    void execute();
    void scruter();
    void reinitialiser();

};


#endif //PROJETHORDES_TOURGUET_HPP
