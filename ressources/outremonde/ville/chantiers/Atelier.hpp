// Created by jozereau on 10/02/17.
//

#ifndef PROJETHORDES_ATELIER_HPP
#define PROJETHORDES_ATELIER_HPP


#include "../../../divers/donnees.hpp"
#include "Construction.hpp"

typedef std::vector<std::pair<std::vector <ObjetId>, ObjetId>> listeConversion; // Destination - Origine

class Atelier : public Construction {
private:
    listeConversion schemas;
    int reductionPa;

public:
    Atelier();
    ~Atelier();

    // Getteurs et setteurs
    int getReductionPa() const {
        return reductionPa;
    }
    void setReductionPa(int reductionPa) {
        Atelier::reductionPa = reductionPa;
    }
    // Fin getteurs et setteurs

    listeConversion ressourcesTransformables();
    std::vector<ObjetId> ressourcesUtiles(Construction *cons);
    void transformer(ObjetId ressource);
    void updateCouts();
};


#endif //PROJETHORDES_ATELIER_HPP
