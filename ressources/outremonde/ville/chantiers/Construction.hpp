//
// Created by jozereau on 08/12/16.
//

#ifndef PROJETHORDES_CONSTRUCTION_HPP
#define PROJETHORDES_CONSTRUCTION_HPP


#include <string>
#include <vector>
#include "../../../divers/donnees.hpp"

class Construction;

typedef  std::vector<std::pair<ObjetId, int>> listeMateriaux;
typedef  std::vector<ConstructionId> listePrerequis;

class Construction {
protected:
    ConstructionId id;
    std::string nom;

    int defense;
    int paTotaux;
    int resistance;

    bool temporaire;
    bool construit;

    // Informations de construction
    listeMateriaux materiaux;
    listePrerequis prerequis;
    int paRestants;

    bool evolutif;
    int niveau;
    int niveauMax;
    bool constructible;

    // Méthodes privées
    void evolutionPompe();
    void evolutionGrandFosse();
    void evolutionAquaTourelles();


public:
    Construction(ConstructionId id, std::string nom, int defense, int paTotaux, bool temporaire, listeMateriaux materiaux,
                 listePrerequis prerequis = listePrerequis(), bool evolutif = false, int niveauMax = 5);
    virtual ~Construction();

    // Getteurs et setteurs
    const ConstructionId &getId() const {
        return id;
    }
    void setId(const ConstructionId &id) {
        Construction::id = id;
    }

    const std::string &getNom() const {
        return nom;
    }
    void setNom(const std::string &nom) {
        Construction::nom = nom;
    }

    int getDefense() const {
        return defense;
    }
    void setDefense(int defense) {
        Construction::defense = defense;
    }

    int getResistance() const {
        return resistance;
    }
    void setResistance(int resistance) {
        Construction::resistance = resistance;
    }

    bool isTemporaire() const {
        return temporaire;
    }
    void setTemporaire(bool temporaire) {
        Construction::temporaire = temporaire;
    }

    bool isConstruit() const {
        return construit;
    }
    void setConstruit(bool construit) {
        Construction::construit = construit;
    }

    const listeMateriaux &getMateriaux() const {
        return materiaux;
    }
    void setMateriaux(const listeMateriaux &materiaux) {
        Construction::materiaux = materiaux;
    }

    const listePrerequis &getPrerequis() const {
        return prerequis;
    }
    void setPrerequis(const listePrerequis &prerequis) {
        Construction::prerequis = prerequis;
    }

    int getPaRestants() const {
        return paRestants;
    }
    void setPaRestants(int paRestants) {
        Construction::paRestants = paRestants;
    }

    int getPaTotaux() const {
        return paTotaux;
    }
    void setPaTotaux(int paTotaux) {
        Construction::paTotaux = paTotaux;
    }

    bool isEvolutif() const {
        return evolutif;
    }
    void setEvolutif(bool evolutif) {
        Construction::evolutif = evolutif;
    }

    int getNiveau() const {
        return niveau;
    }

    bool isConstructible() const {
        return constructible;
    }
    void setConstructible(bool constructible) {
        Construction::constructible = constructible;
    }
    // Fin getteurs et setteurs

    std::vector<Construction *> getPrerequisNonConstruits();
    std::vector<Construction *> getPrerequisConstructibles();

    bool estConstructible();
    int enConstruction(int pa);
    virtual void estConstruit();

    float getCoutTotal();
    int getCoutRation();

    void detruire();

    bool isNiveauMax();
    void evoluer();
};

#endif //PROJETHORDES_CONSTRUCTION_HPP
