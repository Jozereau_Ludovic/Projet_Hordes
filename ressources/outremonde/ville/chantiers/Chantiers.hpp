//
// Created by jozereau on 08/12/16.
//

#ifndef PROJETHORDES_CHANTIERS_HPP
#define PROJETHORDES_CHANTIERS_HPP


#include "Construction.hpp"
#include "Atelier.hpp"
#include "TourGuet.hpp"
#include "../../../divers/Singleton.hpp"

class Chantiers : public Singleton<Chantiers> {
    friend class Singleton<Chantiers>;

private:
    Chantiers();
    ~Chantiers();

    std::vector<Construction *> constructions; // Toutes les constructions
    Construction *recommande;

    // Constructions particulières
    Atelier *atelier;
    TourGuet *tourGuet;

    // Méthodes privées
        // Gestion chantier recommandé
    Construction *choixRecommande();
    Construction *choixPompe();
    Construction *choixDefenses();
    Construction *choixTactique();

        // Gestion évolution
    void choixEvolution();

        // Gestion chantiers particuliers
        // Chantiers temporaires
    void detruireChantiersTemporaires();

        // Cimetière cadenassé
    void defensesCimetiere();

        // Piston calibré
    void fermeturePortes();

        // Productions
    void productions();
    void potager();
    void pamplemoussesExplosifs();

        // Supports défensifs
    void supportsDefensifs();

        // Mise à jour des défenses
    void miseAJourDefenses();
    void constructionsFinies();

    // Fin méthodes privées

public:
    // Getteurs et setteurs
    const std::vector<Construction *> &getConstructions() const {
        return constructions;
    }

    void setConstructions(const std::vector<Construction *> &constructions) {
        Chantiers::constructions = constructions;
    }

    Construction *getRecommande() const {
        return recommande;
    }

    void setRecommande(Construction *recommande) {
        Chantiers::recommande = recommande;
    }

    Atelier *getAtelier() const {
        return atelier;
    }

    void setAtelier(Atelier *atelier) {
        Chantiers::atelier = atelier;
    }

    TourGuet *getTourGuet() const {
        return tourGuet;
    }
    void setTourGuet(TourGuet *tourGuet) {
        Chantiers::tourGuet = tourGuet;
    }
    // Fin des getteurs et setteurs

    void changementJournee(bool avantAttaque);

    std::vector<Construction *> getConstructibles();
    Construction *getConstructionParId(ConstructionId id);

    std::vector<Construction *> getChantiersEvolutifsConstruits();


    std::vector<ConstructionId> chantiersConstruits();


};

#endif //PROJETHORDES_CHANTIERS_HPP
