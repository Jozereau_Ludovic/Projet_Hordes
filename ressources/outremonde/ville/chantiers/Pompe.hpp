//
// Created by jozereau on 02/03/17.
//

#ifndef PROJETHORDES_POMPE_HPP
#define PROJETHORDES_POMPE_HPP


#include "Construction.hpp"

class Pompe : public Construction {
private:
    int eauSuppl;

public:
    Pompe(ConstructionId id, std::string nom, int eauSuppl, int paTotaux, bool temporaire, listeMateriaux materiaux,
                            listePrerequis prerequis = listePrerequis(), bool evolutif = false);

    // Getteurs et setteurs
    int getEauSuppl() const {
        return eauSuppl;
    }

    void setEauSuppl(int eauSuppl) {
        Pompe::eauSuppl = eauSuppl;
    }
    //Fin getteurs et setteurs


    void estConstruit();

};


#endif //PROJETHORDES_POMPE_HPP
