//
// Created by jozereau on 17/10/16.
//

#ifndef PROJETHORDES_PORTE_HPP
#define PROJETHORDES_PORTE_HPP

#include <memory>
#include "../../divers/Singleton.hpp"

class Portes : public Singleton<Portes> {
    friend class Singleton<Portes>;

private:
    Portes();
    ~Portes();

    bool ouvertes; // Si les portes sont ouvertes
    bool detruites; // Si les portes sont détruites
    bool ouvrables; // Si un chantier ne bloque pas l'ouverture

public:
    // Getteurs et setteurs
    bool isOuvertes() const {
        return ouvertes;
    }

    void setOuvertes(bool ouvertes) {
        Portes::ouvertes = ouvertes;
    }

    bool isDetruites() const {
        return detruites;
    }

    void setDetruites(bool detruites) {
        Portes::detruites = detruites;
    }

    bool isOuvrables() const {
        return ouvrables;
    }

    void setOuvrables(bool ouvrables) {
        Portes::ouvrables = ouvrables;
    }
    // Fin getteurs et setteurs
};


#endif //PROJETHORDES_PORTE_HPP
