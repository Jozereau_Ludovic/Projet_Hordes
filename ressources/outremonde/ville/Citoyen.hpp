//
// Created by jozereau on 17/10/16.
//

#ifndef PROJETHORDES_CITOYEN_HPP
#define PROJETHORDES_CITOYEN_HPP


#include <string>
#include "../../comportement/Comportement.hpp"
#include "../../divers/outils.hpp"
#include "Habitation.hpp"
#include "../../objets/Consommable.hpp"
#include "../Case.hpp"
#include "../../objets/Arme.hpp"

class Citoyen {
private:
    int id;
    std::string nom;
    Comportement *comportement;

    int posX;
    int posY;
    bool enVille;

    // PA (PC plus tard)
    int paMax;
    int pa; // Fatigue à 0

    // Inventaire
    listeObjet inventaire;
    int capacite; // 4 sans extensions, 7 au max

    int ration; //Indique le nombre de rations prise ce jour

    bool banni;

    // Etats (goulification non implémentée)
    std::vector<Blessure> blessures; // On peut avoir plusieurs blessures
    Eau eau; // Etat évolutif
    bool desaltere;
    Alcool alcool; // Etat évolutif

    bool rassasie; // A mangé ce jour

    bool clair; // N'a jamais consommé de la drogue
    bool drogue; // A consommé de la drogue ce jour
    bool dependant; // Doit consommer une drogue par jour

    bool campeurAvise; // A campé dehors la nuit, bonus de fouille

    bool terreur; // Est terrorisé

    bool immunise; // Est immunisé contre l'infection et la goulification

    bool infecte; // Est infecté
    bool convalescense; // A été soigné dans la journée

    Habitation *habitation;

    // Méthodes privées
        // Gestion des états
    void remettrePa(int pa_bonus);

        // Gestion des objets
    void ramasserObjet(Objet *objet);
    void prendreBanque(ObjetId objet);
    void libererInventaire(Objet *objet);
    void consommer(Consommable *consommable);
    void mettreObjetsBanque();

        // Gestion des déplacements
    void seDeplacer(Direction direction);

    // Fin méthodes privées

public:
    Citoyen(int id, std::string nom);
    ~Citoyen();

    // Getteurs et setteurs
    int getId() const {
        return id;
    }
    void setId(int id) {
        Citoyen::id = id;
    }

    const std::string &getNom() const {
        return nom;
    }
    void setNom(const std::string &nom) {
        Citoyen::nom = nom;
    }

    Comportement *getComportement() const {
        return comportement;
    }
    void setComportement(Comportement *comportement) {
        Citoyen::comportement = comportement;
    }

    int getPosX() const {
        return posX;
    }
    void setPosX(int posX) {
        Citoyen::posX = posX;
    }

    int getPosY() const {
        return posY;
    }
    void setPosY(int posY) {
        Citoyen::posY = posY;
    }

    bool isEnVille() const {
        return enVille;
    }
    void setEnVille(bool enVille) {
        Citoyen::enVille = enVille;
    }

    int getPaMax() const {
        return paMax;
    }
    void setPaMax(int paMax) {
        Citoyen::paMax = paMax;
    }

    int getPa() const {
        return pa;
    }
    void setPa(int pa) {
        Citoyen::pa = pa;
    }

    const listeObjet &getInventaire() const {
        return inventaire;
    }
    void setInventaire(const listeObjet &inventaire) {
        Citoyen::inventaire = inventaire;
    }

    int getCapacite() const {
        return capacite;
    }
    void setCapacite(int capacite) {
        Citoyen::capacite = capacite;
    }

    int getRation() const {
        return ration;
    }
    void setRation(int ration) {
        Citoyen::ration = ration;
    }

    bool isBanni() const {
        return banni;
    }
    void setBanni(bool banni) {
        Citoyen::banni = banni;
    }

    const std::vector<Blessure> &getBlessures() const {
        return blessures;
    }
    void setBlessures(const std::vector<Blessure> &blessures) {
        Citoyen::blessures = blessures;
    }

    const Eau &getEau() const {
        return eau;
    }
    void setEau(const Eau &eau) {
        Citoyen::eau = eau;
    }

    bool isDesaltere() const {
        return desaltere;
    }
    void setDesaltere(bool desaltere) {
        Citoyen::desaltere = desaltere;
    }

    const Alcool &getAlcool() const {
        return alcool;
    }
    void setAlcool(const Alcool &alcool) {
        Citoyen::alcool = alcool;
    }

    bool isRassasie() const {
        return rassasie;
    }
    void setRassasie(bool rassasie) {
        Citoyen::rassasie = rassasie;
    }

    bool isClair() const {
        return clair;
    }
    void setClair(bool clair) {
        Citoyen::clair = clair;
    }

    bool isDrogue() const {
        return drogue;
    }
    void setDrogue(bool drogue) {
        Citoyen::drogue = drogue;
    }

    bool isDependant() const {
        return dependant;
    }
    void setDependant(bool dependant) {
        Citoyen::dependant = dependant;
    }

    bool isCampeurAvise() const {
        return campeurAvise;
    }
    void setCampeurAvise(bool campeurAvise) {
        Citoyen::campeurAvise = campeurAvise;
    }

    bool isTerreur() const {
        return terreur;
    }
    void setTerreur(bool terreur) {
        Citoyen::terreur = terreur;
    }

    bool isImmunise() const {
        return immunise;
    }
    void setImmunise(bool immunise) {
        Citoyen::immunise = immunise;
    }

    bool isInfecte() const {
        return infecte;
    }
    void setInfecte(bool infecte) {
        Citoyen::infecte = infecte;
    }

    bool isConvalescense() const {
        return convalescense;
    }
    void setConvalescense(bool convalescense) {
        Citoyen::convalescense = convalescense;
    }

    Habitation *getHabitation() const {
        return habitation;
    }
    void setHabitation(Habitation *habitation) {
        Citoyen::habitation = habitation;
    }
    // Fin getteurs et setteurs

    // Méthodes publiques
        // Méthodes globales
    void execute();
    bool changementJournee();

        // Gestion de l'inventaire
    bool restePlaceInventaire();
    Objet *getInventaire(ObjetId objet);

    bool getManger();
    bool getBoire();
    bool getArme();
    bool getDrogue();

        // Gestion des objets
    void utiliserObjet(ObjetId objet);

    void poserBanque(Objet *objet, bool mort = false);
    void prendreCoffre(ObjetId objet);
    void poserCoffre(Objet *objet);

    void prendreObjetRare();

    void prendreRation();
    void prendreEauBanque();
    void prendreNourriture();
    void prendreDrogue();
    void prendreAlcool();
    void prendreArme();

        // Utilisation des objets
    void manger();
    void boire();
    void seDroguer();
    void boireAlcool();
    void attaquer();

        // Gestion de l'extérieur
    void ramasserObjetDehors(Objet *objet);
    void poserObjetDehors(Objet *objet);
    void fouiller();

        // Gestion des déplacements
    void ouvrirPortes();
    void fermerPortes();
    void sortir();
    void entrer();
    void seDeplacer(Case *position);

        // Gestion des chantiers
    void construireChantier(Construction *cons, int pa);
    bool transformerRessources(ObjetId ressource);

        // Gestion de la mort
    void mourir(MortId mort);

    // Fin méthodes publiques
};


#endif //PROJETHORDES_CITOYEN_HPP
