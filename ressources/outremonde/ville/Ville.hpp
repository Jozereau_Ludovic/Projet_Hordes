//
// Created by jozereau on 17/10/16.
//

#ifndef PROJETHORDES_VILLE_HPP
#define PROJETHORDES_VILLE_HPP

#include "Portes.hpp"
#include "Banque.hpp"
#include "Puits.hpp"
#include "chantiers/Chantiers.hpp"
#include "Citoyen.hpp"
#include "../../Expedition.hpp"

class Ville {
private:
    Ville(std::vector<Citoyen *>);
    ~Ville();
    static Ville *ville;

    Portes *portes;
    Banque *banque;
    Puits *puits;
    Chantiers *chantiers;
    std::vector<Citoyen *> citoyens;

    int defenses;

    std::vector<Expedition *> expeditions;

public:
    static Ville *getVille(std::vector<Citoyen *> citoyens = std::vector<Citoyen *>());
    static void killVille();

    // Getteurs et setteurs
    Portes *getPortes() const {
        return portes;
    }
    void setPortes(Portes *portes) {
        Ville::portes = portes;
    }

    Banque *getBanque() const {
        return banque;
    }
    void setBanque(Banque *banque) {
        Ville::banque = banque;
    }

    Puits *getPuits() const {
        return puits;
    }
    void setPuits(Puits *puits) {
        Ville::puits = puits;
    }

    Chantiers *getChantiers() const {
        return chantiers;
    }
    void setChantiers(Chantiers *chantiers) {
        Ville::chantiers = chantiers;
    }

    const std::vector<Citoyen *> &getCitoyens() const {
        return citoyens;
    }
    void setCitoyens(const std::vector<Citoyen *> &citoyens) {
        Ville::citoyens = citoyens;
    }

    int getDefenses() const {
        return defenses;
    }
    void setDefenses(int defenses) {
        Ville::defenses = defenses;
    }

    const std::vector<Expedition *> &getExpeditions() const {
        return expeditions;
    }

    void setExpeditions(const std::vector<Expedition *> &expeditions) {
        Ville::expeditions = expeditions;
    }
    // Fin getteurs et setteurs

    void brasserOrdre();

    void changementJournee();

    bool actionsTerminees();
    bool citoyenVivant();
    void citoyenMort(int id);

    void ajouterExpedition(Expedition *expe);
    void supprimerExpedition(int id);
};


#endif //PROJETHORDES_VILLE_HPP
