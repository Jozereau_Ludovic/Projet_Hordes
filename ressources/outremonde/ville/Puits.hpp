//
// Created by jozereau on 18/10/16.
//

#ifndef PROJETHORDES_PUITS_HPP
#define PROJETHORDES_PUITS_HPP


#include "../../objets/Consommable.hpp"
#include "chantiers/Chantiers.hpp"

class Puits : public Singleton<Puits> {
    friend class Singleton<Puits>;

private:
    Puits();
    ~Puits();

    int quantite; // Nombre de rations d'eau disponibles dans le puits
    bool pompe; // Indique si la pompe est construite et si l'on peut prendre deux rations par jours

public:

    // Getteurs et setteurs
    int getQuantite() const {
        return quantite;
    }

    void setQuantite(int quantite) {
        Puits::quantite = quantite;
    }

    bool isPompe() {
        pompe = Chantiers::getInstance()->getConstructionParId(ConstructionId::pompe)->isConstruit();
        return pompe;
    }

    void setPompe(bool pompe) {
        Puits::pompe = pompe;
    }
    // Fin getteurs et setteurs

    Consommable *prendreRation();

    bool isVide() {
        return quantite == 0;
    }
};


#endif //PROJETHORDES_PUITS_HPP
