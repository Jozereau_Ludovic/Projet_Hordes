//
// Created by jozereau on 18/10/16.
//

#ifndef PROJETHORDES_HABITATION_HPP
#define PROJETHORDES_HABITATION_HPP

#include "../../divers/outils.hpp"

class Habitation {
private:
    //Ajout des travaux plus tard
    int defense;

    listeObjet coffre; // 4 places

public:
    Habitation();
    ~Habitation();

    // Getteurs et setteurs
    int getDefense() const {
        return defense;
    }

    void setDefense(int defense) {
        Habitation::defense = defense;
    }

    const listeObjet &getCoffre() const {
        return coffre;
    }

    void setCoffre(const listeObjet &coffre) {
        Habitation::coffre = coffre;
    }
    // Fin getteurs et setteurs

    bool restePlaceCoffre();
};


#endif //PROJETHORDES_HABITATION_HPP
