//
// Created by dyag on 20/10/16.
//

#ifndef PROJETHORDES_CASE_HPP
#define PROJETHORDES_CASE_HPP

#include "../divers/fonctionGlobale.hpp"
#include "Batiment.hpp"
#include "../divers/donnees.hpp"
#include "../divers/outils.hpp"

class Case {
protected:
    int x;
    int y;

    int nbObjet;
    int zombies;
    int newZombie;
    bool epuisee;
    bool batiment;

    int distPa;

    Batiment *ruine;

    listeObjet sol;

    bool caseControlee;
    int tempsControle;
    int pointControle;

public:
    Case(int x, int y);
    ~Case();

    // Getteurs et setteurs
    int getX() const {
        return x;
    }
    void setX(int x) {
        Case::x = x;
    }

    int getY() const {
        return y;
    }
    void setY(int y) {
        Case::y = y;
    }
    int getDistPa() const {
        return distPa;
    }
    void setDistPa(int distPa) {
        Case::distPa = distPa;
    }

    bool isEpuisee() const {
        return epuisee;
    }

    void setEpuisee(bool epuisee) {
        Case::epuisee = epuisee;
    }

    bool isCaseControlee() const {
        return caseControlee;
    }
    void setCaseControlee(bool caseControlee) {
        Case::caseControlee = caseControlee;
    }

    int getTempsControle() const {
        return tempsControle;
    }
    void setTempsControle(int tempsControle) {
        Case::tempsControle = tempsControle;
    }

    int getPointControle() const {
        return pointControle;
    }
    void setPointControle(int pointControle) {
        Case::pointControle = pointControle;
    }
    // Fin getteurs et setteurs

    void changerPointControle(int ajout);
    bool isControlee();


    const bool getBatiment();
    void setBatiment();
    const int getZombies();
    void setZombies (int x);
    const int getNewZombies();
    void setNewZombies(int x);

    bool isVide();

    void tuerZombie(int nb);

    Objet* fouille();
    void deposerObjet(Objet* obj);
    listeObjet getObjetAuSol();
    void ramasserObjet(Objet *objet);

    bool rareteSuperieure(listeObjet inventaire);
};


#endif //PROJETHORDES_CASE_HPP
