#include <cmath>
#include "ressources/outremonde/OutreMonde.hpp"

#include <fstream>


//Todo: extensions de sac, points de controle

int main() {
    int seed = time(0);

    srand(seed/*1488984785*/);

    std::cout << seed << std::endl << std::endl;

    OutreMonde::getInstance();

    while (Ville::getVille()->citoyenVivant()) {
        Ville::getVille()->brasserOrdre();

        while (!Ville::getVille()->actionsTerminees()) {
            for (Citoyen *citoyen : Ville::getVille()->getCitoyens()) {
                citoyen->execute();
                //std::cout << citoyen->getNom() << " " << (int) citoyen->getComportement()->getProchaineAction() << std::endl;
                //std::cout << citoyen->getNom() << " " << citoyen->getPosX() << ", " << citoyen->getPosY() << std::endl;
            }
            OutreMonde::getInstance()->avancerTemps();
        }
    }

    OutreMonde::destroy();

    return 0;
}